﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Litium;
using Litium.Customers;
using Litium.Foundation.GUI;
using Litium.Foundation.Modules.CMS;
using Litium.Products;
using Litium.Accelerator.Constants;
using Litium.Foundation.Modules.ECommerce.ShoppingCarts;

namespace Litium.Accelerator.Utilities
{
    /// <summary>
    /// Storage utility class for storing person information in session.
    /// </summary>
    public static class PersonStorage
    {
        private const string _hasUserApproverRole = "HasUserApproverRole";
        private const string _hasUserPlacerRole = "HasUserPlacerRole";
        private const string _selectedOrganization = "SelectedOrganization";
        private const string _userPriceLists = "UserPirceLists";

        /// <summary>
        /// Gets or sets the <see cref="Organization" /> stored in session. This <see cref="Organization" /> contains
        /// information about selected organization for the logged in customer.
        /// </summary>
        public static Organization CurrentSelectedOrganization
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session?[_selectedOrganization] != null)
                {
                    return IoC.Resolve<OrganizationService>().Get((Guid)HttpContext.Current.Session[_selectedOrganization]);
                }

                return null;
            }
            set
            {
                if (HttpContext.Current != null)
                {
                    if (value == null)
                    {
                        HttpContext.Current.Session.Remove(_selectedOrganization);
                        HttpContext.Current.Session.Remove(_hasUserApproverRole);
                        HttpContext.Current.Session.Remove(_hasUserPlacerRole);
                        HttpContext.Current.Session.Remove(_userPriceLists);
                    }
                    else
                    {
                        HttpContext.Current.Session[_selectedOrganization] = value.SystemId;
                        HttpContext.Current.Session[_userPriceLists] = GetUserPriceLists(value.SystemId);
                        var person = IoC.Resolve<PersonService>().Get(FoundationContext.Current.UserID);
                        var organizationLink = person.OrganizationLinks.FirstOrDefault(x => x.OrganizationSystemId == value.SystemId);
                        var roles = new List<Role>();
                        if (organizationLink != null)
                        {
                            roles = organizationLink.RoleSystemIds.Select(x => IoC.Resolve<RoleService>().Get(x)).ToList();
                        }

                        var hasplacerRole = roles.Exists(item => item.Id == RolesConstants.RoleOrderPlacer);
                        var hasApproverRole = roles.Exists(item => item.Id == RolesConstants.RoleOrderApprover);

                        HttpContext.Current.Session[_hasUserApproverRole] = hasApproverRole;
                        HttpContext.Current.Session[_hasUserPlacerRole] = hasplacerRole;

                        var orderCarrier = IoC.Resolve<CartAccessor>().Cart.OrderCarrier;
                        //update the current order carrier organization Id.
                        if (orderCarrier.CustomerInfo != null)
                        {
                            orderCarrier.CustomerInfo.OrganizationID = value.SystemId;
                            if (orderCarrier.CustomerInfo.Address != null)
                            {
                                orderCarrier.CustomerInfo.Address.OrganizationName = value.Name;
                            }
                        }
                    }
                }
            }
        }

        public static List<Guid> UserPriceLists
        {
            get
            {
                if (HttpContext.Current?.Session?[_userPriceLists] != null)
                {
                    return (List<Guid>)HttpContext.Current.Session[_userPriceLists];
                }

                return new List<Guid>();
            }
        }

        /// <summary>
        ///     Define if user has approver role
        /// </summary>
        public static bool HasApproverRole
        {
            get
            {
                if (HttpContext.Current?.Session?[_hasUserApproverRole] != null)
                {
                    return (bool)HttpContext.Current.Session[_hasUserApproverRole];
                }

                return false;
            }
        }
        /// <summary>
        ///     Define if user has placer role
        /// </summary>
        public static bool HasPlacerRole
        {
            get
            {
                if (HttpContext.Current?.Session?[_hasUserPlacerRole] != null)
                {
                    return (bool)HttpContext.Current.Session[_hasUserPlacerRole];
                }

                return false;
            }
        }

        private static List<Guid> GetUserPriceLists(Guid organizationSystemId)
        {
            if (organizationSystemId == Guid.Empty) return new List<Guid>();
            return IoC.Resolve<PriceListService>().GetAll().Where(x => x.OrganizationLinks?.Count == 0 || x.OrganizationLinks.Any(y => y.OrganizationSystemId == organizationSystemId)).Select(z => z.SystemId).ToList();
        }
    }
}