﻿using System;
using System.Web;
using Litium.Foundation.Modules.ECommerce.Carriers;

namespace Litium.Accelerator.Utilities
{
    public static class CheckoutState
    {
        public static AddressCarrier Address
        {
            get { return (AddressCarrier)HttpContext.Current.Session["CheckoutAddress"]; }
            set { HttpContext.Current.Session["CheckoutAddress"] = value; }
        }

        public static bool AlternativeAddressEnabled
        {
            get
            {
                if (HttpContext.Current.Session["CheckoutAlternativeAddressEnabled"] == null)
                {
                    HttpContext.Current.Session["CheckoutAlternativeAddressEnabled"] = false;
                }
                return (bool)HttpContext.Current.Session["CheckoutAlternativeAddressEnabled"];
            }
            set { HttpContext.Current.Session["CheckoutAlternativeAddressEnabled"] = value; }
        }

        public static string Comments
        {
            get
            {
                if (HttpContext.Current.Session["CheckoutComments"] == null)
                {
                    HttpContext.Current.Session["CheckoutComments"] = string.Empty;
                }
                return (string)HttpContext.Current.Session["CheckoutComments"];
            }
            set { HttpContext.Current.Session["CheckoutComments"] = value; }
        }

        public static string CustomerSsn
        {
            get
            {
                if (HttpContext.Current.Session["CustomerSsn"] == null)
                {
                    HttpContext.Current.Session["CustomerSsn"] = string.Empty;
                }
                return (string)HttpContext.Current.Session["CustomerSsn"];
            }
            set { HttpContext.Current.Session["CustomerSsn"] = value; }
        }
        public static AddressCarrier DeliveryAddress
        {
            get { return (AddressCarrier)HttpContext.Current.Session["CheckoutDeliveryAddress"]; }
            set { HttpContext.Current.Session["CheckoutDeliveryAddress"] = value; }
        }
        public static bool NeedToRegister
        {
            get
            {
                if (HttpContext.Current.Session["CheckoutNeedToRegister"] == null)
                {
                    HttpContext.Current.Session["CheckoutNeedToRegister"] = false;
                }
                return (bool)HttpContext.Current.Session["CheckoutNeedToRegister"];
            }
            set { HttpContext.Current.Session["CheckoutNeedToRegister"] = value; }
        }

        public static void ClearState()
        {
            Address = null;
            DeliveryAddress = null;
            AlternativeAddressEnabled = false;
            Comments = string.Empty;
            CustomerSsn = string.Empty;
        }

        public static void CopyAddressValues(AddressCarrier addressSource, AddressCarrier deliveryAddressSource)
        {
            CopyAddressValues(addressSource);
            CopyDeliveryAddressValues(deliveryAddressSource);
        }

        public static void CopyAddressValues(AddressCarrier source)
        {
            if (Address == null)
            {
                Address = new AddressCarrier();
            }
            CopyAddress(source, Address);
        }

        public static void CopyDeliveryAddressValues(AddressCarrier source)
        {
            if (DeliveryAddress == null)
            {
                DeliveryAddress = new AddressCarrier();
            }
            CopyDeliveryAddress(source, DeliveryAddress);
        }

        private static void CopyAddress(AddressCarrier source, AddressCarrier target)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }

            target.Email = source.Email ?? target.Email;
            target.Fax = source.Fax ?? target.Fax;
            target.MobilePhone = source.MobilePhone ?? target.MobilePhone;
            target.OrganizationName = source.OrganizationName ?? target.OrganizationName;
            target.Phone = source.Phone ?? target.Phone;

            target.FirstName = source.FirstName ?? target.FirstName;
            target.LastName = source.LastName ?? target.LastName;

            target.CareOf = source.CareOf;
            target.Address1 = source.Address1;
            target.Address2 = source.Address2;
            target.CareOf = source.CareOf;
            target.City = source.City;
            target.Country = source.Country;
            target.State = source.State;
            target.Zip = source.Zip;
        }

        private static void CopyDeliveryAddress(AddressCarrier source, AddressCarrier target)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }

            target.Email = source.Email ?? target.Email;
            target.Fax = source.Fax ?? target.Fax;
            target.MobilePhone = source.MobilePhone ?? target.MobilePhone;
            target.OrganizationName = source.OrganizationName ?? target.OrganizationName;
            target.Phone = source.Phone ?? target.Phone;

            target.FirstName = source.FirstName ?? target.FirstName;
            target.LastName = source.LastName ?? target.LastName;

            target.CareOf = source.CareOf;
            target.Address1 = source.Address1;
            target.Address2 = source.Address2;
            target.City = source.City;
            target.Country = source.Country;
            target.State = source.State;
            target.Zip = source.Zip;
        }
    }
}
