﻿using System;
using Litium.Accelerator.ViewModels;
using Litium.Runtime.DependencyInjection;

namespace Litium.Accelerator.Builders
{
    [Service(Lifetime = DependencyLifetime.Singleton)]
    public abstract class PageModelBuilder<T> : IViewModelBuilder<T>
        where T : PageViewModel
    {
        public abstract T Build(Guid systemId, DataFilterBase dataFilter = null);
    }
}
