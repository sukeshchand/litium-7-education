﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Litium.Accelerator.Constants;
using Litium.Accelerator.Extensions;
using Litium.Accelerator.Routing;
using Litium.Accelerator.Search;
using Litium.Accelerator.ViewModels.Framework;
using Litium.Common;
using Litium.Foundation.Modules.CMS.Search;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Foundation.Modules.ProductCatalog.Routing;
using Litium.Foundation.Search;
using Litium.Framework.Search;
using Litium.Globalization;
using Litium.Products;
using Litium.Web;
using Litium.Web.Models.Globalization;
using Litium.Web.Models.Websites;
using Litium.Web.Routing;
using Litium.Websites;

namespace Litium.Accelerator.Builders.Search
{
    public class SubNavigationViewModelBuilder : IViewModelBuilder<NavigationViewModel>
    {
        private readonly RequestModelAccessor _requestModelAccessor;
        private readonly RouteRequestInfoAccessor _routeRequestInfoAccessor;
        private readonly RouteRequestLookupInfoAccessor _routeRequestLookupInfoAccessor;
        private readonly CategoryService _categoryService;
        private readonly MarketService _marketService;
        private readonly PageService _pageService;
        private readonly UrlService _urlService;
        private readonly SearchQueryBuilderFactory _searchQueryBuilderFactory;
        private readonly SearchService _searchService;

        private List<Guid> _selectedStructureId;
        private Guid _currentCategorySystemId;
        private WebsiteModel _website;
        private ChannelModel _channel;
        private PageModel _page;
        private Guid _currentSelectedPageId;

        public SubNavigationViewModelBuilder(
            RequestModelAccessor requestModelAccessor,
            RouteRequestInfoAccessor routeRequestInfoAccessor,
            RouteRequestLookupInfoAccessor routeRequestLookupInfoAccessor,
            CategoryService categoryService,
            MarketService marketService,
            PageService pageService,
            UrlService urlService,
            SearchQueryBuilderFactory searchQueryBuilderFactory,
            SearchService searchService)
        {
            _requestModelAccessor = requestModelAccessor;
            _routeRequestInfoAccessor = routeRequestInfoAccessor;
            _routeRequestLookupInfoAccessor = routeRequestLookupInfoAccessor;
            _categoryService = categoryService;
            _marketService = marketService;
            _pageService = pageService;
            _urlService = urlService;
            _searchQueryBuilderFactory = searchQueryBuilderFactory;
            _searchService = searchService;
            _website = _requestModelAccessor.RequestModel.WebsiteModel;
            _channel = _requestModelAccessor.RequestModel.ChannelModel;
            _page = _requestModelAccessor.RequestModel.CurrentPageModel;
        }

        /// <summary>
        /// Buid Sub Navigation Model
        /// </summary>
        /// <param name="currentPageModel"></param>
        /// <returns></returns>
        public NavigationViewModel Build()
        {
            var subNavigation = new NavigationViewModel();
            var filterNavigation = _website.GetNavigationType();
            var productCatalogData = _routeRequestInfoAccessor.RouteRequestInfo?.Data as ProductPageData;
            var pageTypeName = _page.GetPageType();

            // sub navigation
            if (productCatalogData != null)
            {
                subNavigation = filterNavigation == NavigationType.Category ? CreateProductCategoryNavigation(productCatalogData, pageTypeName) : CreateProductFilterNavigation(productCatalogData, pageTypeName);
            }
            else if (pageTypeName != PageTemplateNameConstants.Brand && pageTypeName != PageTemplateNameConstants.ProductList 
                && pageTypeName != PageTemplateNameConstants.SearchResult)
            {
                subNavigation = CreatePageNavigation();
            }

            return subNavigation;
        }

        private NavigationViewModel CreateProductCategoryNavigation(ProductPageData productCatalogData, string pageTypeName)
        {
            var category = _categoryService.Get(productCatalogData.CategorySystemId.Value);
            if (category == null)
            {
                return null;
            }

            _selectedStructureId = category.GetParents().Select(x => x.SystemId).ToList();
            _currentCategorySystemId = category.SystemId;
            _selectedStructureId.Add(_currentCategorySystemId);

            var market = _marketService.Get(_channel.Channel.MarketSystemId.Value);
            var firstLevelCategories = _categoryService
                .GetChildCategories(Guid.Empty, market.AssortmentSystemId)
                .Where(x => x.ChannelLinks.Any(z => z.ChannelSystemId == _channel.SystemId))
                .ToList();
            if (firstLevelCategories.Count == 0)
            {
                return null;
            }

            return new NavigationViewModel() {
                ContentLinks = new[]
                {
                    new ContentLinkModel
                    {
                        IsSelected = true,
                        Name = _website.Texts.GetValue("ProductCategories") ?? "Product Categories",
                        Links = firstLevelCategories.Select(x => new ContentLinkModel
                        {
                            IsSelected = _selectedStructureId.Contains(x.SystemId),
                            Name = x.Localizations.CurrentCulture.Name,
                            Url = x.GetUrl(_website.SystemId, _channel.SystemId),
                            Links = _selectedStructureId.Contains(x.SystemId)
                                ? GetChildLinks(x).ToList()
                                : (x.GetChildren(_website.SystemId, true, channelSystemId: _channel.SystemId).Any() ? new List<ContentLinkModel>() : null)
                        }).ToList()
                    }
                }
            };
        }

        private IEnumerable<ContentLinkModel> GetChildLinks(Category category)
        {
            var res = new List<ContentLinkModel>();

            foreach (var child in category
                .GetChildren(_website.SystemId, true, channelSystemId: _channel.SystemId)
                .Where(x => x.Fields.GetValue<bool>("ShowInMenu") && x.IsPublished(_website.SystemId, _channel.SystemId)))
            {
                var link = new ContentLinkModel
                {
                    Name = child.Localizations.CurrentCulture.Name,
                    Url = child.GetUrl(_website.SystemId, _channel.SystemId)
                };

                if (_selectedStructureId.Contains(child.SystemId))
                {
                    link.IsSelected = true;
                    link.Links = GetChildLinks(child).ToList();
                }
                else if (child.GetChildren(_website.SystemId, true, channelSystemId: _channel.SystemId).Any())
                {
                    link.Links = new List<ContentLinkModel>();
                }
                res.Add(link);
            }

            return res;
        }

        private NavigationViewModel CreateProductFilterNavigation(ProductPageData productCatalogData, string pageTypeName)
        {
            var category = _categoryService.Get(productCatalogData.CategorySystemId.Value);
            if (category == null)
            {
                return null;
            }

            _selectedStructureId = category.GetParents().Select(x=>x.SystemId).ToList();
            _currentCategorySystemId = category.SystemId;
            _selectedStructureId.Add(category.SystemId);

            var market = _marketService.Get(_channel.Channel.MarketSystemId.Value);
            var firstLevelCategories = _categoryService.GetChildCategories(Guid.Empty, market.AssortmentSystemId).Where(x => x.IsPublished(_website.SystemId, _channel.SystemId));

            var currentCategoryParentSystemId = category.ParentCategorySystemId;
            return new NavigationViewModel()
            {
                ContentLinks = new[]
                {
                    new ContentLinkModel
                    {
                        IsSelected = true,
                        Name = _website.Texts.GetValue("ProductCategories") ?? "Product Categories",
                        Links = firstLevelCategories
                            .Where(x => currentCategoryParentSystemId == Guid.Empty || _selectedStructureId.Contains(x.SystemId))
                            .Select(x =>
                            {
                                var showAll = currentCategoryParentSystemId == Guid.Empty && _selectedStructureId.Contains(x.SystemId);
                                return new ContentLinkModel
                                {
                                    IsSelected = _selectedStructureId.Contains(x.SystemId),
                                    Name = x.Localizations.CurrentCulture.Name,
                                    Url =  x.GetUrl(_website.SystemId, _channel.SystemId),
                                    Links = _selectedStructureId.Contains(x.SystemId) ? 
                                    GetChildLinks(x, showAll, showAll ? 0 : 1).ToList() : 
                                    (x.GetChildren(_website.SystemId, true, channelSystemId: _channel.SystemId).Any() ? new List<ContentLinkModel>() : null)
                                };
                            }).ToList()
                    }
                }
            };
        }

        private IEnumerable<ContentLinkModel> GetChildLinks(Category category, bool showAll = false, int level = int.MaxValue)
        {
            var res = new List<ContentLinkModel>();

            foreach (var child in category.GetChildren(_website.SystemId, true, channelSystemId: _channel.SystemId).Where(pg => pg.IsPublished(_website.SystemId, _channel.SystemId)))
            {
                if (showAll || _selectedStructureId.Contains(child.SystemId))
                {
                    var link = new ContentLinkModel
                    {
                        Name = child.Localizations.CurrentCulture.Name,
                        Url = child.GetUrl(_website.SystemId, _channel.SystemId),
                        IsSelected = _selectedStructureId.Contains(child.SystemId),
                        Links = _selectedStructureId.Contains(child.SystemId) ? 
                                GetChildLinks(child, _currentCategorySystemId == child.SystemId, level + 1).ToList() : 
                                (category.GetChildren(_website.SystemId, false, channelSystemId: _channel.SystemId).Any() ? new List<ContentLinkModel>() : null)
                    };

                    res.Add(link);
                }
            }

            return res;
        }

        private NavigationViewModel CreatePageNavigation()
        {
            _currentSelectedPageId = _page.SystemId;
            _selectedStructureId = _page.GetAncestors().Select(c => c.SystemId).ToList();

            if (!_selectedStructureId.Any())
            {
                return null; // startpage, don't select the menu
            }

            _selectedStructureId.Insert(0, _page.SystemId);

            var rootPage = _selectedStructureId.Count > 1
                ? _pageService.Get(_selectedStructureId.Skip(_selectedStructureId.Count - 2).First())
                : _page.Page;

            var rootNavigationLink = new ContentLinkModel()
            {
                IsSelected = _currentSelectedPageId == rootPage.SystemId,
                Name = rootPage.Localizations.CurrentCulture.Name,
                Url = _urlService.GetUrl(rootPage, new PageUrlArgs(_channel.SystemId)),
                Links = GetChildLinks(rootPage).ToList()
            };

            return new NavigationViewModel()
            {
                ContentLinks = new[] { rootNavigationLink }
            };
        }

        private IEnumerable<ContentLinkModel> GetChildLinks(Page rootPage)
        {
            var pageTypeName = rootPage.GetPageType();
            if (pageTypeName == PageTemplateNameConstants.NewsList)
            {
                return GetNewsChildLinks(rootPage, pageTypeName);
            }

            if (pageTypeName == PageTemplateNameConstants.BrandList)
            {
                return new List<ContentLinkModel>();
            }

            return GetRegularChildLinks(rootPage).ToList();
        }

        private IEnumerable<ContentLinkModel> GetNewsChildLinks(Page rootPage, string pageTypeName)
        {
            var searchQuery = _requestModelAccessor.RequestModel.SearchQuery.Clone();
            searchQuery.PageSize = 5;
            searchQuery.PageNumber = 1;
            var searchQueryBuilder = _searchQueryBuilderFactory.Create(CultureInfo.CurrentUICulture, CmsSearchDomains.Pages, searchQuery);
            var request = searchQueryBuilder.Build();
            request.FilterTags.Add(new Tag(TagNames.Status, (int)ContentStatus.Published));
            request.FilterTags.Add(new Tag(TagNames.PageParentTreeId, rootPage.SystemId));
            request.FilterTags.Add(new Tag(TagNames.TemplateId, PageTemplateNameConstants.News));
            request.Sortings.Add(new Sorting(TagNames.SortIndex, SortDirection.Descending, SortingFieldType.Int));

            var result = _searchService.Search(request);

            return _pageService.Get(result.Hits.Select(x => new Guid(x.Id)).ToList())
                       .Select(child =>
                           new ContentLinkModel
                           {
                               Name = child.Localizations.CurrentCulture.Name,
                               Url = _urlService.GetUrl(child),
                               IsSelected = _currentSelectedPageId == child.SystemId
                           })
                       .ToList();
        }

        private IEnumerable<ContentLinkModel> GetRegularChildLinks(Page rootPage)
        {
            var res = new List<ContentLinkModel>();
            foreach (var child in _pageService.GetChildPages(rootPage.SystemId).Where(x => x.Status == ContentStatus.Published))
            {
                var link = new ContentLinkModel
                {
                    Name = child.Localizations.CurrentCulture.Name,
                    Url = _urlService.GetUrl(child),
                    IsSelected = _currentSelectedPageId == child.SystemId
                };

                if (_selectedStructureId.Contains(child.SystemId))
                {
                    link.Links = GetChildLinks(child).ToList();
                }
                else if (_pageService.GetChildPages(child.SystemId).Any(x => x.Status == ContentStatus.Published))
                {
                    link.Links = new List<ContentLinkModel>();
                }

                res.Add(link);
            }

            return res;
        }
    }
}
