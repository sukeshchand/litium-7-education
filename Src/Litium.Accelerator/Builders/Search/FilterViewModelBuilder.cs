﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Litium.Accelerator.Constants;
using Litium.Accelerator.Extensions;
using Litium.Accelerator.Routing;
using Litium.Accelerator.Search;
using Litium.Accelerator.Search.Filtering;
using Litium.Accelerator.ViewModels.Brand;
using Litium.Accelerator.ViewModels.Search;
using Litium.FieldFramework;
using Litium.FieldFramework.FieldTypes;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Foundation.Modules.ProductCatalog.ExtensionMethods;
using Litium.Foundation.Modules.ProductCatalog.Search;
using Litium.Framework.Search;
using Litium.Globalization;
using Litium.Products;
using Litium.Studio.Extenssions;
using Litium.Web;
using Litium.Web.Products.Routing;
using Litium.Web.Routing;

namespace Litium.Accelerator.Builders.Search
{
    public class FilterViewModelBuilder : IViewModelBuilder<FilterResult>
    {
        private static readonly TagComparer _tagComparer = new TagComparer();

        private readonly RequestModelAccessor _requestModelAccessor;
        private readonly RouteRequestInfoAccessor _routeRequestInfoAccessor;
        private readonly CategoryFilterService _categoryFilterService;
        private readonly ProductSearchService _productSearchService;
        private readonly PriceFilterService _priceFilterService;
        private readonly FilterService _filterService;
        private readonly CategoryService _categoryService;
        private readonly CurrencyService _currencyService;
        private readonly UrlService _urlService;

        public FilterViewModelBuilder(
            RequestModelAccessor requestModelAccessor,
            CategoryFilterService categoryFilterService,
            ProductSearchService productSearchService,
            PriceFilterService priceFilterService,
            FilterService filterService,
            CategoryService categoryService,
            CurrencyService currencyService,
            RouteRequestInfoAccessor routeRequestInfoAccessor,
            UrlService urlService)
        {
            _requestModelAccessor = requestModelAccessor;
            _categoryFilterService = categoryFilterService;
            _productSearchService = productSearchService;
            _priceFilterService = priceFilterService;
            _filterService = filterService;
            _categoryService = categoryService;
            _currencyService = currencyService;
            _routeRequestInfoAccessor = routeRequestInfoAccessor;
            _urlService = urlService;
        }

        public FilterResult Build()
        {
            var filterNavigation = _requestModelAccessor.RequestModel.WebsiteModel.GetNavigationType() == NavigationType.Filter;
            var searchQuery = _requestModelAccessor.RequestModel.SearchQuery;
            var productCatalogData = _routeRequestInfoAccessor.RouteRequestInfo?.Data as ProductPageData;
            var pageModel = _requestModelAccessor.RequestModel.CurrentPageModel;
            if (productCatalogData == null && !pageModel.IsBrandPageType() && !pageModel.IsProductListPageType() && !pageModel.IsSearchResultPageType())
            {
                return null;
            }
            
            if ((searchQuery.Type != SearchType.Products && !filterNavigation) || (pageModel.IsSearchResultPageType() && string.IsNullOrEmpty(searchQuery.Text)))
            {
                return null;
            }

            var propertyNames = GetPropertyNames(searchQuery);

            var fullSearch = _productSearchService.Search(searchQuery, null, false, false, false);
            if (fullSearch == null)
            {
                return null;
            }

            var allTagTerms = fullSearch.Hits
                                        .SelectMany(x => x.Tags.Distinct(_tagComparer))
                                        .ToLookup(x => x.Name, StringComparer.OrdinalIgnoreCase)
                                        .ToDictionary(x => x.Key, x => x
                                            .ToLookup(z => z.Value, StringComparer.OrdinalIgnoreCase)
                                            .ToDictionary(z => z.Key, z => z.Count(), StringComparer.OrdinalIgnoreCase), StringComparer.OrdinalIgnoreCase);

            var result = new FilterResult
            {
                Items = new List<FilterItem>()
            };

            foreach (var propertyName in propertyNames)
            {
                if (propertyName.Equals(FilteringConstants.FilterPrice, StringComparison.OrdinalIgnoreCase))
                {
                    var filterTag = GetPriceTag(searchQuery, fullSearch, true, _requestModelAccessor.RequestModel.CountryModel.Country.CurrencySystemId);
                    if (filterTag != null)
                    {
                        result.Items.Add(filterTag);
                    }
                }
                else if (propertyName.Equals(FilteringConstants.FilterNews, StringComparison.OrdinalIgnoreCase))
                {
                    if (allTagTerms.Count > 0)
                    {
                        var filterTag = GetNewsTag(searchQuery);
                        if (filterTag != null)
                        {
                            result.Items.Add(filterTag);
                        }
                    }
                }
                else if (propertyName.Equals(FilteringConstants.FilterProductCategories, StringComparison.OrdinalIgnoreCase))
                {
                    var filterTag = GetProductCategoryTag(searchQuery, allTagTerms);
                    if (filterTag != null)
                    {
                        result.Items.Add(filterTag);
                    }
                }
                else
                {
                    var filterTag = GetFilterTag(searchQuery, propertyName, allTagTerms, true);
                    if (filterTag != null)
                    {
                        result.Items.Add(filterTag);
                    }
                }
            }

            return result;
        }

        public FilterItem GetFilterTag(SearchQuery searchQuery, string propertyName, Dictionary<string, Dictionary<string, int>> allTagTerms, bool includeCount)
        {
            var field = propertyName.GetFieldDefinitionForProducts();
            if (field == null)
            {
                return null;
            }

            var tagName = field.GetTagName(CultureInfo.CurrentCulture);

            if (allTagTerms.TryGetValue(tagName, out Dictionary<string, int> tagValues) && tagValues.Count > 0)
            {
                Dictionary<string, int> currentTagSearchResult = null;
                if (!searchQuery.Tags.TryGetValue(propertyName, out ISet<string> selectedValues))
                {
                    selectedValues = new SortedSet<string>(StringComparer.OrdinalIgnoreCase);
                }

                var containsFilter = searchQuery.ContainsFilter();
                if (includeCount && containsFilter)
                {
                    currentTagSearchResult = _productSearchService
                        .Search(searchQuery, searchQuery.Tags.Where(x => !x.Key.Equals(propertyName, StringComparison.OrdinalIgnoreCase)).ToDictionary(x => x.Key, x => x.Value), true, true, true)
                        .Hits
                        .SelectMany(x => x.Tags.Distinct(_tagComparer))
                        .Where(x => x.Name.Equals(tagName, StringComparison.OrdinalIgnoreCase))
                        .Select(x => x.Value)
                        .ToLookup(x => x, StringComparer.OrdinalIgnoreCase)
                        .ToDictionary(x => x.Key, x => x.Count(), StringComparer.OrdinalIgnoreCase);
                }

                return new FilterItem
                {
                    Name = field.Localizations.CurrentCulture.Name ?? field.Id,
                    Attributes = new Dictionary<string, string>
                    {
                        { "filter-name", propertyName.ToLowerInvariant() },
                        { "filter-base-url", searchQuery.GetUrlTag(propertyName, null, false) }
                    },
                    IsSelected = searchQuery.Tags.ContainsKey(propertyName),
                    Url = searchQuery.GetUrlTag(propertyName, null, false),
                    Links = tagValues
                        .Select(x =>
                        {
                            string extraInfo = null;
                            var count = 0;
                            if (includeCount)
                            {
                                if (currentTagSearchResult != null)
                                {
                                    count = currentTagSearchResult.TryGetValue(x.Key, out count) ? count : 0;
                                }
                                else
                                {
                                    count = x.Value;
                                }
                                extraInfo = count.ToString(CultureInfo.CurrentCulture.NumberFormat);
                            }

                            string key;
                            switch (field.FieldType)
                            {
                                case SystemFieldTypeConstants.Decimal:
                                case SystemFieldTypeConstants.Int:
                                    {
                                        key = x.Key.TrimStart('0');
                                        break;
                                    }
                                case SystemFieldTypeConstants.Date:
                                case SystemFieldTypeConstants.DateTime:
                                    {
                                        if (long.TryParse(x.Key, NumberStyles.Any, CultureInfo.InvariantCulture, out long l))
                                        {
                                            key = new DateTime(l).ToShortDateString();
                                        }
                                        else
                                        {
                                            goto default;
                                        }
                                        break;
                                    }
                                default:
                                    {
                                        key = x.Key;
                                        break;
                                    }
                            }

                            var selected = selectedValues.Any(z => x.Key.Equals(z, StringComparison.InvariantCultureIgnoreCase));

                            return new FilterItem
                            {
                                Name = key,
                                IsSelected = selected,
                                ExtraInfo = extraInfo,
                                Url = searchQuery.GetUrlTag(propertyName, x.Key, selected),
                                Count = count,
                                Attributes = new Dictionary<string, string>
                                {
                                    { "value", x.Key.ToLowerInvariant() },
                                    { "cssValue", (FindValue(field, key, CultureInfo.CurrentCulture) ?? key)?.ToLowerInvariant() },
                                    { "count", count.ToString(CultureInfo.InvariantCulture) },
                                    { "rel", containsFilter || !_filterService.IndexFilter(propertyName) ? "nofollow" : null }
                                }
                            };
                        })
                        .OrderByDescending(x => x.Count)
                        .ThenBy(x => x.Name)
                        .ToList()
                };
            }

            return null;
        }

        public FilterItem GetNewsTag(SearchQuery searchQuery)
        {
            var dteValue1 = new Tuple<DateTime, DateTime>(DateTime.Today.AddMonths(-1), DateTime.Today);
            var dteValue3 = new Tuple<DateTime, DateTime>(DateTime.Today.AddMonths(-3), DateTime.Today);
            var containsFilter = searchQuery.ContainsFilter();

            return new FilterItem
            {
                Attributes = new Dictionary<string, string>
                {
                    { "filter-name", "news" },
                    { "filter-base-url", searchQuery.GetUrlNews(null, true) }
                },
                Name = "filter.newsheadline".AsWebSiteString(),
                Url = searchQuery.GetUrlNews(null, true),
                SingleSelect = true,
                Links = new List<FilterItem>(new[]
                {
                    new FilterItem
                    {
                        Name = "filter.newslastmonth".AsWebSiteString(),
                        Url = searchQuery.GetUrlNews(dteValue1, Equals(dteValue1, searchQuery.NewsDate)),
                        IsSelected = Equals(dteValue1, searchQuery.NewsDate),
                        Attributes = new Dictionary<string, string>
                        {
                            { "value", string.Format("{0:yyyyMMdd}-{1:yyyyMMdd}", dteValue1.Item1, dteValue1.Item2) },
                            { "rel", containsFilter || !_filterService.IndexFilter(FilteringConstants.FilterNews) ? "nofollow" : null }
                        }
                    },
                    new FilterItem
                    {
                        Name = "filter.newslast3month".AsWebSiteString(),
                        Url = searchQuery.GetUrlNews(dteValue3, Equals(dteValue3, searchQuery.NewsDate)),
                        IsSelected = Equals(dteValue3, searchQuery.NewsDate),
                        Attributes = new Dictionary<string, string>
                        {
                            { "value", string.Format("{0:yyyyMMdd}-{1:yyyyMMdd}", dteValue3.Item1, dteValue3.Item2) },
                            { "rel", containsFilter || !_filterService.IndexFilter(FilteringConstants.FilterNews) ? "nofollow" : null }
                        }
                    }
                })
            };
        }

        public FilterItem GetPriceTag(SearchQuery searchQuery, SearchResponse fullSearch, bool includeChild, Guid currencySystemId)
        {
            var currency = _currencyService.Get(currencySystemId);
            var currentTagSearchResult = fullSearch
                .Hits
                .Cast<ProductSearchService.SortableSearchHit>()
                .Select(x => x.Price)
                .ToLookup(x => x)
                .ToDictionary(x => x.Key, x => x.Count());

            if (currentTagSearchResult.Count > 1)
            {
                var keys = currentTagSearchResult.Keys.Where(x => x > decimal.MinusOne).ToArray();
                var minPrice = keys.Length > 0 ? (int)Math.Abs(keys.Min()) : 0;
                var maxPrice = keys.Length > 0 ? (int)Math.Floor(keys.Max()) : 0;
                var containsFilter = searchQuery.ContainsFilter();

                return new FilterItem
                {
                    Name = "filter.price".AsWebSiteString(),
                    Attributes = new Dictionary<string, string>
                    {
                        { "filter-name", "price_range" },
                        { "filter-base-url", searchQuery.GetUrlPrice() },
                        { "filter-min-price", minPrice.ToString(CultureInfo.InvariantCulture) },
                        { "filter-max-price", maxPrice.ToString(CultureInfo.InvariantCulture) }
                    },
                    Url = searchQuery.GetUrlPrice(),
                    IsSelected = searchQuery.ContainsPriceFilter(),
                    Links = includeChild
                        ? CreatePriceGroup(currentTagSearchResult, minPrice, maxPrice)
                            .Select(x =>
                            {
                                var extraInfo = x.Item3.ToString(CultureInfo.CurrentCulture.NumberFormat);
                                var currentPriceRange = new Tuple<int, int>(x.Item1, x.Item2);
                                var selected = searchQuery.PriceRanges.Contains(currentPriceRange);
                                return new FilterItem
                                {
                                    Name = string.Format("{0}-{1}", FormatPrice(currency, x.Item1), FormatPrice(currency, x.Item2)),
                                    IsSelected = selected,
                                    ExtraInfo = extraInfo,
                                    Count = int.Parse(extraInfo),
                                    Url = searchQuery.GetUrlPrice(currentPriceRange, selected),
                                    Attributes = new Dictionary<string, string>
                                    {
                                        { "value", string.Format("{0}-{1}", x.Item1, x.Item2) },
                                        { "count", extraInfo },
                                        { "rel", containsFilter || !_filterService.IndexFilter(FilteringConstants.FilterPrice) ? "nofollow" : null }
                                    }
                                };
                            })
                            .ToList()
                        : null
                };
            }

            return null;
        }

        public FilterItem GetProductCategoryTag(SearchQuery searchQuery, Dictionary<string, Dictionary<string, int>> allTagTerms)
        {
            if (allTagTerms.TryGetValue(TagNames.CategorySystemId, out Dictionary<string, int> tags))
            {
                Dictionary<string, int> currentTagSearchResult = null;
                if (searchQuery.ContainsFilter())
                {
                    currentTagSearchResult = _productSearchService
                        .Search(searchQuery, searchQuery.Tags, true, true, false)
                        .Hits
                        .SelectMany(x => x.Tags.Distinct(_tagComparer))
                        .Where(x => x.Name.Equals(TagNames.CategorySystemId, StringComparison.OrdinalIgnoreCase))
                        .Select(x => x.Value)
                        .ToLookup(x => x, StringComparer.OrdinalIgnoreCase)
                        .ToDictionary(x => x.Key, x => x.Count(), StringComparer.OrdinalIgnoreCase);
                }

                var containsFilter = searchQuery.ContainsFilter();
                return new FilterItem
                {
                    Attributes = new Dictionary<string, string>
                    {
                        { "filter-name", "category" },
                        { "filter-base-url", searchQuery.GetUrlCategory(null, true) }
                    },
                    Name = "filter.productcategories".AsWebSiteString(),
                    Url = searchQuery.GetUrlCategory(null, true),
                    Links = new List<FilterItem>(tags
                        .Select(x => new { Category = _categoryService.Get(new Guid(x.Key)), Tags = x })
                        .Where(x => !string.IsNullOrEmpty(_urlService.GetUrl(x.Category)))
                        .Select(x =>
                        {
                            var isSelected = searchQuery.Category.Contains(x.Category.SystemId);
                            int count = currentTagSearchResult != null ? (currentTagSearchResult.TryGetValue(x.Tags.Key, out count) ? count : 0) : x.Tags.Value;
                            var extraInfo = count.ToString(CultureInfo.CurrentCulture.NumberFormat);

                            return new FilterItem
                            {
                                Name = x.Category.Fields.GetValue<string>(SystemFieldDefinitionConstants.Name, CultureInfo.CurrentCulture),
                                Url = searchQuery.GetUrlCategory(x.Category.SystemId, isSelected),
                                Count = x.Tags.Value,
                                ExtraInfo = extraInfo,
                                IsSelected = isSelected,
                                Attributes = new Dictionary<string, string>
                                {
                                    { "value", x.Category.SystemId.ToString("N") },
                                    { "rel", containsFilter || !_filterService.IndexFilter(FilteringConstants.FilterProductCategories) ? "nofollow" : null }
                                }
                            };
                        })
                        .OrderByDescending(x => x.Count)
                        .ThenBy(x => x.Name))
                };
            }

            return null;
        }

        private static IEnumerable<Tuple<int, int, int>> CreatePriceGroup(Dictionary<decimal, int> priceHits, int minPrice, int maxPrice)
        {
            const decimal slots = 7;

            var roundedMaxPrice = Round(maxPrice, true);
            var priceInEachInterval = (int)Math.Floor(roundedMaxPrice / slots);

            var result = new List<Tuple<int, int, int>>();
            var lastPrice = Round(minPrice, false);
            for (var i = 1; lastPrice < maxPrice; i++)
            {
                var i1 = i * priceInEachInterval;
                var maxSlotPrice = Round(i1, true);
                var items = priceHits.Where(x => x.Key >= lastPrice && x.Key <= maxSlotPrice).Sum(x => x.Value);

                //Rounding off can make lastPrice and maxSlotPrice have the same value 
                if (items > 0 && lastPrice != maxSlotPrice)
                {
                    result.Add(new Tuple<int, int, int>(lastPrice, maxSlotPrice, items));
                }

                lastPrice = maxSlotPrice;
            }

            return result;
        }

        private static string FindValue(FieldDefinition fieldDefinition, string term, CultureInfo cultureInfo)
        {
            if (fieldDefinition.FieldType == SystemFieldTypeConstants.TextOption)
            {
                var option = fieldDefinition.Option as TextOption;

                return option?.Items.FirstOrDefault(x => x.Name.TryGetValue(cultureInfo.Name, out string value)
                    && term.Equals(value, StringComparison.OrdinalIgnoreCase))?.Value;
            }

            return term;
        }

        private static string FormatPrice(Currency currency, int value)
        {
            return currency.Format(value, false, CultureInfo.CurrentUICulture);
        }

        private static int Round(int value, bool roundUp)
        {
            var roundLevels = new[]
            {
                // param 1: price below, param 2: round to nerest
                new[] { 100, 10 },
                new[] { 1000, 100 },
                new[] { 5000, 500 },
                new[] { 10000, 1000 },
                new[] { 15000, 1500 },
                new[] { 20000, 2000 },
                new[] { 25000, 2500 },
                new[] { 50000, 5000 },
                new[] { 100000, 10000 },
                new[] { 150000, 15000 },
                new[] { 500000, 50000 },
                new[] { 1000000, 100000 },
                new[] { 10000000, 1000000 },
                new[] { int.MaxValue, 10000000 }
            };
            foreach (var item in roundLevels)
            {
                if (value <= item[0])
                {
                    if (roundUp)
                    {
                        return value + item[1] - (value % item[1]);
                    }

                    return value - (value % item[1]);
                }
            }

            throw new ArgumentException("No matching roundings for actual value.", nameof(value));
        }

        private IEnumerable<string> GetPropertyNames(SearchQuery searchQuery)
        {
            var filters = _requestModelAccessor.RequestModel.WebsiteModel.Fields.GetValue<IList<string>>(AcceleratorWebsiteFieldNameConstants.FiltersOrdering);
            if (filters == null)
            {
                return new string[0];
            }

            if (searchQuery.CategorySystemId != null)
            {
                var result = _categoryFilterService.GetFilters(searchQuery.CategorySystemId.Value);
                if (result != null)
                {
                    return result
                        .Select(x => new Tuple<string, int>(x, filters.IndexOf(x)))
                        .Where(x => x.Item2 != -1)
                        .OrderBy(x => x.Item2)
                        .Select(x => x.Item1)
                        .ToList();
                }
            }
            var page = _requestModelAccessor.RequestModel.CurrentPageModel;

            //Current page is the search result pagetype
            if (page.IsSearchResultPageType())
            {
                return new[] { FilteringConstants.FilterProductCategories }.Concat(filters);
            }

            //Current page is the brand pagetype
            if (page.IsBrandPageType())
            {
                return filters.Except(new[] { BrandListViewModel.TagName }, StringComparer.OrdinalIgnoreCase);
            }

            return filters;
        }

        private class TagComparer : IEqualityComparer<Tag>
        {
            public bool Equals(Tag x, Tag y)
            {
                return x.Name == y.Name && Equals(x.OriginalValue, y.OriginalValue);
            }

            public int GetHashCode(Tag obj)
            {
                return obj.Name.GetHashCode();
            }
        }
    }
}
