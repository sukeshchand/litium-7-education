﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Accelerator.Payments;
using Litium.Accelerator.Routing;
using Litium.Accelerator.Services;
using Litium.Accelerator.ViewModels.Checkout;
using Litium.Foundation.Modules.ECommerce;
using Litium.Foundation.Security;

namespace Litium.Accelerator.Builders.Checkout
{
    public class PaymentMethodViewModelBuilder : IViewModelBuilder<PaymentMethodViewModel>
    {
        private readonly ModuleECommerce _moduleECommerce;
        private readonly PaymentService _paymentService;
        private readonly RequestModelAccessor _requestModelAccessor;
        private readonly SecurityToken _securityToken;
        private readonly CartService _cartService;

        public PaymentMethodViewModelBuilder(
            ModuleECommerce moduleECommerce,
            PaymentService paymentService,
            SecurityToken securityToken,
            CartService cartService,
            RequestModelAccessor requestModelAccessor
            )
        {
            _moduleECommerce = moduleECommerce;
            _paymentService = paymentService;
            _requestModelAccessor = requestModelAccessor;
            _securityToken = securityToken;
            _cartService = cartService;
        }

        public PaymentWidgetResult BuildWidget(string paymentMethodId)
        {
            var currentOrderCarrier = _requestModelAccessor.RequestModel.Cart.OrderCarrier;
            var paymentMethodParts = paymentMethodId.Split(':');
            var paymentMethod = _moduleECommerce.PaymentMethods.Get(paymentMethodParts[1], paymentMethodParts[0], _securityToken);
            var paymentInfo = currentOrderCarrier.PaymentInfo.Single(x => x.PaymentProvider.Equals(paymentMethod.PaymentProviderName) && !x.CarrierState.IsMarkedForDeleting);
            var paymentWidget = _paymentService.GetPaymentWidget(paymentInfo);
            return paymentWidget?.GetWidget(currentOrderCarrier, paymentInfo);
        }

        public virtual List<PaymentMethodViewModel> Build()
        {
            var ids = _requestModelAccessor.RequestModel.ChannelModel?.Channel?.CountryLinks?.FirstOrDefault()?.PaymentMethodSystemIds ?? Enumerable.Empty<Guid>();
            var currency = _requestModelAccessor.RequestModel.Cart.Currency;
            var paymentMethods = _moduleECommerce.PaymentMethods.GetAll().Where(x => ids.Contains(x.ID)).Select(x =>
            {
                var cost = x.GetCost(currency.SystemId)?.Cost ?? 0;
                return new PaymentMethodViewModel
                {
                    Id = string.Concat(x.PaymentProviderName, ":", x.Name),
                    Name = x.Name,
                    Price = cost,
                    FormattedPrice = _cartService.FormatAmount(cost)
                };
            }).ToList();
            return paymentMethods;
        }
    }
}
