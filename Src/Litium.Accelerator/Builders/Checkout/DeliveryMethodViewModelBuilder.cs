﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Accelerator.Routing;
using Litium.Accelerator.Services;
using Litium.Accelerator.ViewModels.Checkout;
using Litium.Foundation.Modules.ECommerce;

namespace Litium.Accelerator.Builders.Checkout
{
    public class DeliveryMethodViewModelBuilder : IViewModelBuilder<DeliveryMethodViewModel>
    {
        private readonly ModuleECommerce _moduleECommerce;
        private readonly RequestModelAccessor _requestModelAccessor;
        private readonly CartService _cartService;

        public DeliveryMethodViewModelBuilder(
            RequestModelAccessor requestModelAccessor,
            CartService cartService,
            ModuleECommerce moduleECommerce)
        {
            _moduleECommerce = moduleECommerce;
            _requestModelAccessor = requestModelAccessor;
            _cartService = cartService;
        }

        public virtual List<DeliveryMethodViewModel> Build()
        {
            var ids = _requestModelAccessor.RequestModel.ChannelModel?.Channel?.CountryLinks?.FirstOrDefault()?.DeliveryMethodSystemIds ?? Enumerable.Empty<Guid>();
            var currency = _requestModelAccessor.RequestModel.Cart.Currency;
            var deliveryMethods = _moduleECommerce.DeliveryMethods.GetAll().Where(x => ids.Contains(x.ID)).Select(x =>
            {
                var cost = x.GetCost(currency.SystemId)?.Cost ?? 0;
                return new DeliveryMethodViewModel
               {
                   Id = x.ID,
                   Name = x.Name,
                   Price = cost,
                   FormattedPrice = _cartService.FormatAmount(cost)
               };
            }).ToList();
            return deliveryMethods;
        }
    }
}
