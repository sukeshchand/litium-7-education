﻿using System.Collections.Generic;
using System.Linq;
using Litium.Accelerator.Constants;
using Litium.Accelerator.Routing;
using Litium.Accelerator.ViewModels.Framework;
using Litium.FieldFramework.FieldTypes;
using Litium.Runtime.AutoMapper;
using Litium.Web.Models;

namespace Litium.Accelerator.Builders.Framework
{
    public class FooterViewModelBuilder : IViewModelBuilder<FooterViewModel>
    {
        private readonly RequestModelAccessor _requestModelAccessor;

        public FooterViewModelBuilder(RequestModelAccessor requestModelAccessor)
        {
            _requestModelAccessor = requestModelAccessor;
        }

        public FooterViewModel Build()
        {
            var website = _requestModelAccessor.RequestModel.WebsiteModel;

            return new FooterViewModel
            {
                SectionList = new List<SectionModel>
                {
                    new SectionModel
                    {
                        SectionTitle = website.GetValue<string>(AcceleratorWebsiteFieldNameConstants.FooterHeader1),
                        SectionLinkList = website.GetValue<IList<PointerItem>>(AcceleratorWebsiteFieldNameConstants.FooterLinkList1)?.OfType<PointerPageItem>().ToList().Select(x=>x.MapTo<LinkModel>()).Where(x => x.AccessibleByUser).ToList() ?? new List<LinkModel>(),
                        SectionText = website.GetValue<string>(AcceleratorWebsiteFieldNameConstants.FooterText1),
                    },
                    new SectionModel
                    {
                        SectionTitle = website.GetValue<string>(AcceleratorWebsiteFieldNameConstants.FooterHeader2),
                        SectionLinkList = website.GetValue<IList<PointerItem>>(AcceleratorWebsiteFieldNameConstants.FooterLinkList2)?.OfType<PointerPageItem>().ToList().Select(x=>x.MapTo<LinkModel>()).Where(x => x.AccessibleByUser).ToList() ?? new List<LinkModel>(),
                        SectionText = website.GetValue<string>(AcceleratorWebsiteFieldNameConstants.FooterText2),
                    },
                    new SectionModel
                    {
                        SectionTitle = website.GetValue<string>(AcceleratorWebsiteFieldNameConstants.FooterHeader3),
                        SectionLinkList = website.GetValue<IList<PointerItem>>(AcceleratorWebsiteFieldNameConstants.FooterLinkList3)?.OfType<PointerPageItem>().ToList().Select(x=>x.MapTo<LinkModel>()).Where(x => x.AccessibleByUser).ToList() ?? new List<LinkModel>(),
                        SectionText = website.GetValue<string>(AcceleratorWebsiteFieldNameConstants.FooterText3),
                    },
                    new SectionModel
                    {
                        SectionTitle = website.GetValue<string>(AcceleratorWebsiteFieldNameConstants.FooterHeader4),
                        SectionLinkList = website.GetValue<IList<PointerItem>>(AcceleratorWebsiteFieldNameConstants.FooterLinkList4)?.OfType<PointerPageItem>().ToList().Select(x=>x.MapTo<LinkModel>()).Where(x => x.AccessibleByUser).ToList() ?? new List<LinkModel>(),
                        SectionText = website.GetValue<string>(AcceleratorWebsiteFieldNameConstants.FooterText4),
                    }
                }
            };
        }
    }
}