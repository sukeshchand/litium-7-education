﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Accelerator.Builders.Search;
using Litium.Accelerator.Caching;
using Litium.Accelerator.Constants;
using Litium.Accelerator.Extensions;
using Litium.Accelerator.Routing;
using Litium.Accelerator.Search;
using Litium.Accelerator.Search.Filtering;
using Litium.Accelerator.ViewModels.Framework;
using Litium.Common;
using Litium.FieldFramework.FieldTypes;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Framework.Search;
using Litium.Products;
using Litium.Runtime.AutoMapper;
using Litium.Security;
using Litium.Web;
using Litium.Web.Models;
using Litium.Web.Models.Products;
using Litium.Web.Models.Websites;
using Litium.Web.Products.Routing;
using Litium.Web.Rendering;
using Litium.Web.Routing;
using Litium.Websites;

namespace Litium.Accelerator.Builders.Framework
{
    /// <summary>
    /// Control builds mega menu.
    /// Mega menu could work in three ways.
    /// 1. Shows simple links.
    /// 2. Shows mega menu content.
    /// 3. Shows mega menu subpages.
    /// </summary>
    public class NavigationViewModelBuilder : IViewModelBuilder<NavigationViewModel>
    {
        private readonly Dictionary<Guid, Tuple<SearchQuery, SearchResponse, Dictionary<string, Dictionary<string, int>>>> _filterSearchersCache = new Dictionary<Guid, Tuple<SearchQuery, SearchResponse, Dictionary<string, Dictionary<string, int>>>>();
        private readonly RequestModelAccessor _requestModelAccessor;
        private readonly CategoryService _categoryService;
        private readonly PageService _pageService;
        private readonly UrlService _urlService;
        private readonly PageByFieldTemplateCache<MegaMenuPageFieldTemplateCache> _pageByFieldType;
        private readonly AuthorizationService _authorizationService;
        private readonly List<Guid> _selectedStructureId = new List<Guid>();
        private readonly RouteRequestInfoAccessor _routeRequestInfoAccessor;
        private readonly FilterViewModelBuilder _filterViewModelBuilder;
        private readonly ProductSearchService _productSearchService;
        private readonly FilterService _filterService;
        private readonly ContentProcessorService _contentProcessorService;

        public NavigationViewModelBuilder(RequestModelAccessor requestModelAccessor,
            CategoryService categoryService,
            PageService pageService,
            UrlService urlService,
            PageByFieldTemplateCache<MegaMenuPageFieldTemplateCache> pageByFieldType,
            AuthorizationService authorizationService,
            RouteRequestInfoAccessor routeRequestInfoAccessor,
            FilterViewModelBuilder filterViewModelBuilder,
            ProductSearchService productSearchService,
            FilterService filterService,
            ContentProcessorService contentProcessorService)
        {
            _requestModelAccessor = requestModelAccessor;
            _categoryService = categoryService;
            _pageService = pageService;
            _urlService = urlService;
            _pageByFieldType = pageByFieldType;
            _authorizationService = authorizationService;
            _routeRequestInfoAccessor = routeRequestInfoAccessor;
            _filterViewModelBuilder = filterViewModelBuilder;
            _productSearchService = productSearchService;
            _filterService = filterService;
            _contentProcessorService = contentProcessorService;
        }

        /// <summary>
        /// Builds model
        /// </summary>
        /// <returns></returns>
        public NavigationViewModel Build()
        {
            var model = new NavigationViewModel();
            var megaMenuPages = GetMegaMenuPages();
            var channelSystemId = _requestModelAccessor.RequestModel.ChannelModel.SystemId;
            var websiteSystemId = _requestModelAccessor.RequestModel.WebsiteModel.SystemId;
            foreach (var megaMenuPage in megaMenuPages)
            {
                var linkName = megaMenuPage?.Page?.Localizations.CurrentCulture.Name;
                if (string.IsNullOrEmpty(linkName))
                {
                    continue;
                }
                var contentLinkModel = new ContentLinkModel
                {
                    Name = linkName
                };
                var categoryModel = megaMenuPage.Fields.GetValue<Guid?>(MegaMenuPageFieldNameConstants.MegaMenuCategory)?.MapTo<CategoryModel>();
                PageModel pageModel = null;
                // If a category is chosen, the link will redirect to the category in the first place
                if (categoryModel != null)
                {
                    if (categoryModel.Category != null && categoryModel.Category.IsPublished(websiteSystemId, channelSystemId))
                    {
                        contentLinkModel.Url = _urlService.GetUrl(categoryModel.Category);
                        contentLinkModel.IsSelected = _selectedStructureId.Contains(categoryModel.Category.SystemId);
                    }
                }
                else
                {
                    pageModel = megaMenuPage.Fields.GetValue<PointerPageItem>(MegaMenuPageFieldNameConstants.MegaMenuPage)?.EntitySystemId.MapTo<PageModel>();
                    if (pageModel?.Page != null && _authorizationService.HasOperation<Page>(Operations.Entity.Read, pageModel.Page.SystemId) && pageModel.Page.Status == ContentStatus.Published)
                    {
                        contentLinkModel.Url = megaMenuPage.Fields.GetValue<PointerPageItem>(MegaMenuPageFieldNameConstants.MegaMenuPage).MapTo<LinkModel>().Href;
                        contentLinkModel.IsSelected = _selectedStructureId.Contains(pageModel.Page.SystemId);
                    }
                }
                // Category or Page must be set
                if (categoryModel == null && pageModel == null)
                {
                    continue;
                }

                model.ContentLinks.Add(contentLinkModel);

                if (megaMenuPage.Fields.GetValue<bool>(MegaMenuPageFieldNameConstants.MegaMenuShowContent))
                {
                    if (megaMenuPage.Fields.GetValue<bool>(MegaMenuPageFieldNameConstants.MegaMenuShowSubCategories))
                    {
                        // Show two levels of sub categories/pages
                        contentLinkModel.Links = categoryModel != null ? GetSubCategoryLinks(categoryModel, true) : GetSubPageLinks(pageModel, true);
                    }
                    else
                    {
                        // Render mega menu context
                        contentLinkModel.Links = GetMegaMenuContent(megaMenuPage, categoryModel, websiteSystemId);
                    }
                }
                if (!contentLinkModel.IsSelected)
                {
                    contentLinkModel.IsSelected = IsChildSelected(contentLinkModel);
                }
            }

            var selectedCount = model.ContentLinks.Count(x => x.IsSelected);
            if (selectedCount <= 1)
            {
                return model;
            }
            
            var selectedPagesToClear = model.ContentLinks.Where(x => x.IsSelected).Take(selectedCount - 1);
            foreach (var page in selectedPagesToClear)
            {
                UnSelect(page);
            }
            return model;
        }

        private List<ContentLinkModel> GetMegaMenuContent(PageModel pageModel, CategoryModel categoryModel, Guid websiteSystemId)
        {
            var megaMenuContentLinks = new List<ContentLinkModel>();
            for (var i = 1; i <= 4; i++)
            {
                var contentLink = new ContentLinkModel
                {
                    Name = pageModel.Fields.GetValue<string>(MegaMenuPageFieldNameConstants.MegaMenuColumnHeader + i)
                };

                var links = new List<ContentLinkModel>();
                // Categories has higher priority
                var categories = pageModel.Fields.GetValue<IList<PointerItem>>(MegaMenuPageFieldNameConstants.MegaMenuCategories + i)?.Select(x => x.EntitySystemId.MapTo<CategoryModel>()).ToList();
                if (categories != null)
                {
                    links.AddRange(categories.Select(category => new ContentLinkModel()
                    {
                        Name = category.Category.Localizations.CurrentCulture.Name,
                        Url = _urlService.GetUrl(category.Category),
                        IsSelected = _selectedStructureId.Contains(category.Category.SystemId)
                    }));
                }
                else if (pageModel.Fields.GetValue<IList<PointerItem>>(MegaMenuPageFieldNameConstants.MegaMenuPages + i) != null)
                {
                    var pages = pageModel.Fields.GetValue<IList<PointerItem>>(MegaMenuPageFieldNameConstants.MegaMenuPages + i).OfType<PointerPageItem>().ToList();
                    links.AddRange(pages.Where(x => _authorizationService.HasOperation<Page>(Operations.Entity.Read, x.EntitySystemId))
                        .Select(x => x.EntitySystemId.MapTo<PageModel>())
                        .Where(x => x != null && x.Page.Status == ContentStatus.Published)
                        .Select(contentPageModel => new ContentLinkModel()
                        {
                            Name = contentPageModel.Page.Localizations.CurrentUICulture.Name,
                            Url = pages.First(x => x.EntitySystemId == contentPageModel.SystemId).MapTo<LinkModel>().Href,
                            IsSelected = _selectedStructureId.Contains(contentPageModel.Page.SystemId)
                        }));
                }
                // Filters works just with category
                else if (categoryModel != null && pageModel.Fields.GetValue<IList<string>>(MegaMenuPageFieldNameConstants.MegaMenuFilters + i)!=null)
                {
                    links.AddRange(GetFilters(categoryModel, pageModel.Fields.GetValue<IList<string>>(MegaMenuPageFieldNameConstants.MegaMenuFilters + i).ToList(), websiteSystemId, _requestModelAccessor.RequestModel.CountryModel.Country.CurrencySystemId));
                }
                else if (!string.IsNullOrEmpty(pageModel.Fields.GetValue<string>(MegaMenuPageFieldNameConstants.MegaMenuEditor + i)))
                {
                    links.Add(new ContentLinkModel()
                    {
                        Name = _contentProcessorService.Process(pageModel.Fields.GetValue<string>(MegaMenuPageFieldNameConstants.MegaMenuEditor + i))
                    });
                }

                if (!string.IsNullOrEmpty(pageModel.Fields.GetValue<string>(MegaMenuPageFieldNameConstants.MegaMenuAdditionalLink + i)))
                {
                    var link = string.Empty;
                    var linkToCategoryModel = pageModel.Fields.GetValue<Guid?>(MegaMenuPageFieldNameConstants.MegaMenuLinkToCategory + i)?.MapTo<CategoryModel>();
                    if (linkToCategoryModel != null)
                    {
                        if (linkToCategoryModel.Category != null)
                            link = _urlService.GetUrl(linkToCategoryModel.Category);
                    }
                    else
                    {
                        var linkedPageModel = pageModel.Fields.GetValue<PointerPageItem>(MegaMenuPageFieldNameConstants.MegaMenuLinkToPage + i)?.EntitySystemId.MapTo<PageModel>();
                        if (linkedPageModel?.Page != null && _authorizationService.HasOperation<Page>(Operations.Entity.Read, linkedPageModel.Page.SystemId) && linkedPageModel.Page.Status == ContentStatus.Published)
                        {
                            link = pageModel.Fields.GetValue<PointerPageItem>(MegaMenuPageFieldNameConstants.MegaMenuLinkToPage + i).MapTo<LinkModel>().Href;
                        }
                    }
                    if (!string.IsNullOrEmpty(link))
                    {
                        links.Add(new ContentLinkModel()
                        {
                            Attributes = new Dictionary<string, string>
                            {
                                {
                                    "cssValue", "nav-link"
                                }
                            },
                            Name = pageModel.Fields.GetValue<string>(MegaMenuPageFieldNameConstants.MegaMenuAdditionalLink + i),
                            Url  = link
                        });
                    }
                }
                contentLink.Links = links;
                megaMenuContentLinks.Add(contentLink);
            }

            return megaMenuContentLinks;
        }

        private List<PageModel> GetMegaMenuPages()
        {
            var megaMenuPages = new List<PageModel>();
            _pageByFieldType.TryFindPage(page =>
           {
               if (page != null)
               {
                   var pageModel = page.MapTo<PageModel>();
                   // pageModel can be null if current user does not have access to it
                   // or if the page is not published on the current channel
                   if (pageModel != null)
                   {
                       megaMenuPages.Add(pageModel);
                   }
               }

               return false;
           });
            return megaMenuPages.OrderBy(p => p.Page.SortIndex).ToList();
        }

        private List<ContentLinkModel> GetSubPageLinks(PageModel pageModel, bool showNextLevel)
        {
            var links = new List<ContentLinkModel>();
            foreach (var subPage in _pageService.GetChildPages(pageModel.Page.SystemId, pageModel.Page.WebsiteSystemId).Where(x => x.Status == ContentStatus.Published && _authorizationService.HasOperation<Page>(Operations.Entity.Read, x.SystemId)).Select(x => x.MapTo<PageModel>()).Where(x => x != null))
            {
                var subLinkModel = new ContentLinkModel
                {
                    Name = subPage.Page.Localizations.CurrentUICulture.Name,
                    Url = _urlService.GetUrl(subPage.Page),
                    IsSelected = _selectedStructureId.Contains(subPage.Page.SystemId)
                };
                if (showNextLevel)
                {
                    subLinkModel.Links = GetSubPageLinks(subPage, false);
                }
                links.Add(subLinkModel);
            }

            return links;
        }

        private List<ContentLinkModel> GetSubCategoryLinks(CategoryModel categoryModel, bool showNextLevel)
        {
            var links = new List<ContentLinkModel>();
            foreach (var subCategory in _categoryService.GetChildCategories(categoryModel.Category.SystemId, Guid.Empty).Select(x => x.MapTo<CategoryModel>()))
            {
                var subLinkModel = new ContentLinkModel
                {
                    Name = subCategory.Category.Localizations.CurrentCulture.Name,
                    IsSelected = _selectedStructureId.Contains(subCategory.Category.SystemId),
                    Url = _urlService.GetUrl(subCategory.Category)
                };
                if (showNextLevel)
                {
                    subLinkModel.Links = GetSubCategoryLinks(subCategory, false);
                }
                links.Add(subLinkModel);
            }

            return links;
        }

        private bool IsChildSelected(ContentLinkModel link)
        {
            var anySelected = link.IsSelected;
            if (link.Links == null)
            {
                return anySelected;
            }
            foreach (var child in link.Links)
            {
                if (IsChildSelected(child))
                {
                    anySelected = child.IsSelected = true;
                }
            }

            return anySelected;
        }

        private void UnSelect(ContentLinkModel link)
        {
            link.IsSelected = false;
            if (link.Links == null)
            {
                return;
            }
            
            foreach (var child in link.Links)
            {
                UnSelect(child);
            }
        }

        private void SetSelectedPages(PageModel currentPageModel, Guid websiteSystemId)
        {
            var productCatalogData = _routeRequestInfoAccessor.RouteRequestInfo?.Data as ProductPageData;
            if (productCatalogData != null)
            {
                CategoryModel categoryModel;
                var variant = productCatalogData.VariantSystemId.GetValueOrDefault().GetVariant();

                if (variant != null)
                {
                    _selectedStructureId.Add(variant.SystemId);
                    categoryModel = variant.GetMainCategory(websiteSystemId).MapTo<CategoryModel>();
                }
                else
                {
                    categoryModel = productCatalogData.CategorySystemId?.MapTo<CategoryModel>();
                }

                if (categoryModel != null)
                {
                    _selectedStructureId.AddRange(categoryModel.Category.GetParents().Select(x => x.SystemId));
                    _selectedStructureId.Add(categoryModel.Category.SystemId);
                }
            }
            var pageModel = currentPageModel;
            while (pageModel != null && pageModel.Page.ParentPageSystemId != Guid.Empty)
            {
                _selectedStructureId.Add(pageModel.Page.SystemId);
                pageModel = pageModel.Page.ParentPageSystemId.MapTo<PageModel>();
            }

            _selectedStructureId.Add(currentPageModel.Page.SystemId);
            _selectedStructureId.Remove(_pageService.GetChildPages(Guid.Empty, currentPageModel.Page.WebsiteSystemId).First().SystemId);
        }

        private List<ContentLinkModel> GetFilters(CategoryModel categoryModel, List<string> selectedFilters, Guid websiteSystemId, Guid currencySystemId)
        {
            var result = new List<ContentLinkModel>();
            var fields = _filterService.GetProductFilteringFields().Where(selectedFilters.Contains).ToList();
            SearchQuery searchQuery;
            SearchResponse filterFullSearch;
            Dictionary<string, Dictionary<string, int>> filterTagTerms;
            if (_filterSearchersCache.TryGetValue(categoryModel.Category.SystemId, out var cacheItem))
            {
                searchQuery = cacheItem.Item1;
                filterFullSearch = cacheItem.Item2;
                filterTagTerms = cacheItem.Item3;
            }
            else
            {
                var categoryShowRecursively = websiteSystemId.MapTo<WebsiteModel>().GetNavigationType() == NavigationType.Filter;
                searchQuery = new SearchQuery
                {
                    CategorySystemId = categoryModel.Category.SystemId,
                    CategoryShowRecursively = categoryShowRecursively
                };

                filterFullSearch = _productSearchService.Search(searchQuery);
                filterTagTerms = filterFullSearch.Hits
                                                 .SelectMany(x => x.Tags)
                                                 .ToLookup(x => x.Name, StringComparer.OrdinalIgnoreCase)
                                                 .ToDictionary(x => x.Key, x => x
                                                     .ToLookup(z => z.Value, StringComparer.OrdinalIgnoreCase)
                                                     .ToDictionary(z => z.Key, z => z.Count(), StringComparer.OrdinalIgnoreCase), StringComparer.OrdinalIgnoreCase);

                _filterSearchersCache.Add(categoryModel.Category.SystemId, Tuple.Create(searchQuery, filterFullSearch, filterTagTerms));
            }

            foreach (var item in fields)
            {
                if (item.Equals(FilteringConstants.FilterPrice, StringComparison.OrdinalIgnoreCase))
                {
                    var links = _filterViewModelBuilder.GetPriceTag(searchQuery, filterFullSearch, true, currencySystemId);
                    if (links != null)
                    {
                        result.Add(links.MapTo<ContentLinkModel>());
                    }
                }
                else if (item.Equals(FilteringConstants.FilterNews, StringComparison.OrdinalIgnoreCase))
                {
                    var links = _filterViewModelBuilder.GetNewsTag(searchQuery);
                    if (links != null)
                    {
                        result.Add(links.MapTo<ContentLinkModel>());
                    }
                }
                else
                {
                    var links = _filterViewModelBuilder.GetFilterTag(searchQuery, item, filterTagTerms, false);
                    if (links != null)
                    {
                        result.Add(links.MapTo<ContentLinkModel>());
                    }
                }
            }

            return result;
        }
    }
}
