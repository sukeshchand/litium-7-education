﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Accelerator.Constants;
using Litium.Accelerator.Utilities;
using Litium.Accelerator.ViewModels.Persons;
using Litium.Customers;
using Litium.Runtime.AutoMapper;

namespace Litium.Accelerator.Builders.Person
{
    public class BusinessPersonViewModelBuilder : IViewModelBuilder<BusinessPersonViewModel>
    {
        private readonly PersonService _personService;
        private readonly RoleService _roleService;

        public BusinessPersonViewModelBuilder(PersonService personService, RoleService roleService)
        {
            _personService = personService;
            _roleService = roleService;
        }

        public virtual IEnumerable<BusinessPersonViewModel> Build()
        {
            return GetPersons().MapEnumerableTo<BusinessPersonViewModel>();
        }

        public virtual BusinessPersonViewModel Build(Guid id)
        {
            var person = _personService.Get(id);
            return person.MapTo<BusinessPersonViewModel>();
        }

        private IEnumerable<Customers.Person> GetPersons()
        {
            var currentOrganization = PersonStorage.CurrentSelectedOrganization;
            if (currentOrganization == null)
            {
                return null;
            }

            return _personService.Get(currentOrganization.PersonLinks.Where(x => HasRoles(x, RolesConstants.RoleOrderApprover, RolesConstants.RoleOrderPlacer)).Select(y => y.PersonSystemId));
        }

        private bool HasRoles(OrganizationToPersonLink personLink, params string[] roleIds)
        {
            var roles = roleIds.Select(roleId => _roleService.Get(roleId)).Where(role => role != null);

            return HasRoles(personLink, roles.ToArray());
        }

        private static bool HasRoles(OrganizationToPersonLink personLink, params Role[] roles)
        {
            return roles.Any(role => personLink.RoleSystemIds.Contains(role.SystemId));
        }
    }
}
