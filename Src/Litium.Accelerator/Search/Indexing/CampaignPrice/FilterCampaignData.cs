using System;

namespace Litium.Accelerator.Search.Indexing.CampaignPrice
{
    [Serializable]
    internal class FilterCampaignData
    {
        public Guid CampaignId { get; set; }
        public decimal Price { get; set; }
    }
}
