using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Foundation.Modules.CMS.Search;
using Litium.Web.Customers.TargetGroups;
using Litium.Web.Customers.TargetGroups.Events;
using Litium.Foundation.Search;
using Litium.Framework.Search;
using Litium.Runtime.DependencyInjection;
using Litium.Accelerator.Routing;
using Litium.Web;
using Litium.Accelerator.ViewModels.Search;
using System.Collections.ObjectModel;
using Litium.Websites;
using System.Globalization;
using Litium.Accelerator.Constants;
using Litium.Common;

namespace Litium.Accelerator.Search
{
    [Service(ServiceType = typeof(PageSearchService), Lifetime = DependencyLifetime.Singleton)]
    public class PageSearchService
    {
        private readonly SearchService _searchService;
        private readonly RequestModelAccessor _requestModelAccessor;
        private readonly UrlService _urlService;
        private readonly SearchQueryBuilderFactory _searchQueryBuilderFactory;
        private readonly PageService _pageService;

        public PageSearchService(
            SearchService searchService,
            RequestModelAccessor requestModelAccessor,
            UrlService urlService,
            SearchQueryBuilderFactory searchQueryBuilderFactory,
            PageService pageService)
        {
            _searchService = searchService;
            _requestModelAccessor = requestModelAccessor;
            _urlService = urlService;
            _searchQueryBuilderFactory = searchQueryBuilderFactory;
            _pageService = pageService;
        }

        public SearchResponse Search(SearchQuery searchQuery, bool? onlyBrands = null)
        {
            if (!string.IsNullOrEmpty(searchQuery.Text))
            {
                var websiteId = _requestModelAccessor.RequestModel.WebsiteModel.SystemId;

                var searchQueryBuilder = _searchQueryBuilderFactory.Create(CultureInfo.CurrentUICulture, CmsSearchDomains.Pages, searchQuery);

                searchQueryBuilder.ApplyFreeTextSearchTags();
                searchQueryBuilder.ApplyDefaultSortOrder();

                var request = searchQueryBuilder.Build();
                request.FilterTags.Add(new Tag(TagNames.Status, (int)ContentStatus.Published));
                request.FilterTags.Add(new Tag(TagNames.IsInTrashcan, false));
                request.FilterTags.Add(new Tag(TagNames.WebSiteId, websiteId));
                request.FilterTags.Add(new Tag(TagNames.IsSearchable, true));

                if (onlyBrands != null)
                {
                    var brandPageType = PageTemplateNameConstants.Brand;
                    if (brandPageType == null)
                    {
                        return null;
                    }

                    if (onlyBrands == true)
                    {
                        request.FilterTags.Add(new Tag(TagNames.TemplateId, brandPageType));
                    }
                    else
                    {
                        request.ExcludeTags.Add(new Tag(TagNames.TemplateId, brandPageType));
                    }
                }

                return _searchService.Search(request);
            }

            return null;
        }

        public SearchResult Transform(int pageSize, SearchQuery searchQuery, SearchResponse searchResponse, bool includeScore = false)
        {
            return new SearchResult
            {
                Items = new Lazy<IEnumerable<SearchResultItem>>(() =>
                {
                    IoC.Resolve<TargetGroupEngine>().Process(new SearchEvent
                    {
                        SearchText = searchQuery.Text,
                        TotalHits = searchResponse.TotalHitCount
                    });

                    var result = new Collection<Hit>(searchResponse.Hits.Skip((searchQuery.PageNumber - 1) * pageSize).Take(pageSize).ToList());
                    var pages = _pageService.Get(result.Select(x => new Guid(x.Id)).ToList());
                    return pages.Select(x => new PageSearchResult
                    {
                        Item = x,
                        Id = x.SystemId,
                        Name = x.Localizations.CurrentCulture.Name,
                        Url = _urlService.GetUrl(x),
                        Score = includeScore ? searchResponse.Hits.Where(z => z.Id == x.SystemId.ToString()).Select(z => z.Score).FirstOrDefault() : default(float)
                    });
                }),
                PageSize = pageSize,
                Total = searchResponse.TotalHitCount
            };
        }
    }
}
