using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Foundation.Modules.ProductCatalog.Search;
using Litium.Web.Customers.TargetGroups;
using Litium.Web.Customers.TargetGroups.Events;
using Litium.Foundation.Search;
using Litium.Framework.Search;
using Litium.Runtime.DependencyInjection;
using System.Globalization;
using Litium.Accelerator.ViewModels.Search;
using Litium.Web;
using System.Collections.ObjectModel;

namespace Litium.Accelerator.Search
{
    [Service(ServiceType = typeof(CategorySearchService), Lifetime = DependencyLifetime.Singleton)]
    public class CategorySearchService
    {
        private readonly SearchService _searchService;
        private readonly UrlService _urlService;
        private readonly SearchQueryBuilderFactory _searchQueryBuilderFactory;

        public CategorySearchService(
            SearchService searchService,
            UrlService urlService,
            SearchQueryBuilderFactory searchQueryBuilderFactory)
        {
            _searchService = searchService;
            _urlService = urlService;
            _searchQueryBuilderFactory = searchQueryBuilderFactory;
        }

        public SearchResponse Search(SearchQuery searchQuery)
        {
            if (!string.IsNullOrEmpty(searchQuery.Text))
            {
                var searchQueryBuilder = _searchQueryBuilderFactory.Create(CultureInfo.CurrentUICulture, ProductCatalogSearchDomains.Categories, searchQuery);

                searchQueryBuilder.ApplyProductCatalogDefaultSearchTag();
                searchQueryBuilder.ApplyFreeTextSearchTags();
                searchQueryBuilder.ApplyCategoryTags();
                searchQueryBuilder.ApplySelectedFilterTags();
                searchQueryBuilder.ApplyDefaultSortOrder();

                var request = searchQueryBuilder.Build();
                return _searchService.Search(request);
            }

            return null;
        }

        public SearchResult Transform(int pageSize, SearchQuery searchQuery, SearchResponse searchResponse, bool includeScore = false)
        {
            return new SearchResult
            {
                Items = new Lazy<IEnumerable<SearchResultItem>>(() =>
                {
                    IoC.Resolve<TargetGroupEngine>().Process(new SearchEvent
                    {
                        SearchText = searchQuery.Text,
                        TotalHits = searchResponse.TotalHitCount
                    });

                    var result = new Collection<Hit>(searchResponse.Hits.Skip((searchQuery.PageNumber - 1) * pageSize).Take(pageSize).ToList());
                    var categories = result.Select(x => new Guid(x.Id).GetCategory());
                    return categories.Where(c => c != null).Select(x => new CategorySearchResult
                    {
                        Item = x,
                        Id = x.SystemId,
                        Name = x.Localizations.CurrentCulture.Name,
                        Url = _urlService.GetUrl(x),
                        Score = includeScore ? searchResponse.Hits.Where(z => z.Id == x.SystemId.ToString()).Select(z => z.Score).FirstOrDefault() : default(float)
                    });
                }),
                PageSize = pageSize,
                Total = searchResponse.TotalHitCount
            };
        }
    }
}
