using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Litium.Accelerator.Constants;
using Litium.Common.Events;
using Litium.Events;
using Litium.FieldFramework.Events;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Products.Events;
using Litium.Runtime.DependencyInjection;

namespace Litium.Accelerator.Search.Filtering
{
    [Service(ServiceType = typeof(CategoryFilterService), Lifetime = DependencyLifetime.Singleton)]
    public class CategoryFilterService
    {
        private readonly ConcurrentDictionary<Guid, string[]> _cache = new ConcurrentDictionary<Guid, string[]>();
        private readonly FilterService _filterService;

        public CategoryFilterService(EventBroker eventBroker, FilterService filterService)
        {
            eventBroker.Subscribe<CategoryCreated>(_ => _cache.Clear());
            eventBroker.Subscribe<CategoryDeleted>(_ => _cache.Clear());
            eventBroker.Subscribe<CategoryUpdated>(_ => _cache.Clear());
            eventBroker.Subscribe<FieldDefinitionCreated>(_ => _cache.Clear());
            eventBroker.Subscribe<FieldDefinitionDeleted>(_ => _cache.Clear());
            eventBroker.Subscribe<FieldDefinitionUpdated>(_ => _cache.Clear());
            eventBroker.Subscribe<SettingChanged>(x =>
            {
                if (FilterService._key == x.Key && x.PersonSystemId == Guid.Empty)
                {
                    _cache.Clear();
                }
            });
            _filterService = filterService;
        }

        public string[] GetFilters(Guid categorySystemId)
        {
            return _cache.GetOrAdd(categorySystemId, key =>
            {
                var filterFields = _filterService.GetProductFilteringFields();
                var customFilters = GetCustomFilterValues(key);
                return customFilters != null ? filterFields.Intersect(customFilters, StringComparer.OrdinalIgnoreCase).ToArray() : (filterFields?.ToArray() ?? new string[0]);
            });
        }

        private static IList<string> GetCustomFilterValues(Guid categorySystemId)
        {
            var category = categorySystemId.GetCategory();
            while (category != null)
            {
                var customFilterOptions = category.Fields.GetValue<IList<string>>(NavigationConstants.AcceleratorFilterFieldDefinitionName);
                if (customFilterOptions != null)
                {
                    return customFilterOptions;
                }

                if (category.ParentCategorySystemId == Guid.Empty)
                {
                    break;
                }

                category = category.ParentCategorySystemId.GetCategory();
            }

            return null;
        }
    }
}
