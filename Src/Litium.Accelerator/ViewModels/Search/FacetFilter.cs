﻿using System.Collections.Generic;

namespace Litium.Accelerator.ViewModels.Search
{
    public class FacetFilter
    {
        public string Label { get; set; }
        public string Id { get; set; }
        public int Quantity { get; set; }
        public bool IsSelected { get; set; }
        public string Value { get; set; }
        public string[] SelectedOptions { get; set; }
        public bool SingleSelect { get; set; }
        public List<FacetFilter> Options { get; set; }
    }
}
