﻿using AutoMapper;
using Litium.Runtime.AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace Litium.Accelerator.ViewModels.Search
{
    public class FilterItem : IAutoMapperConfiguration
    {
        public Dictionary<string, string> Attributes { get; set; }
        public int Count { get; set; }
        public string ExtraInfo { get; set; }
        public bool IsSelected { get; set; }
        public bool SingleSelect { get; set; }
        public IList<FilterItem> Links { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }

        void IAutoMapperConfiguration.Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<FilterItem, FacetFilter>()
               .ForMember(x => x.Label, m => m.MapFrom(c => c.Name))
               .ForMember(x => x.Id, m => m.MapFrom((c, _) => c.Attributes.TryGetValue("filter-name", out string value) ? value : ""))
               .ForMember(x => x.Quantity, m => m.MapFrom(c => c.Count))
               .ForMember(x => x.Value, m => m.MapFrom((c, _) => c.Attributes.TryGetValue("value", out string value) ? value : ""))
               .ForMember(x => x.SelectedOptions, m => m.MapFrom((c, _) =>
               {
                   if (c.Links == null)
                       return new string[] { };

                   var childsSelected = c.Links.Where(l => l.IsSelected).ToList();
                   if (childsSelected?.Count > 0)
                   {
                        return childsSelected.Select(l => l.Attributes.TryGetValue("value", out string value) ? value : "");
                   }
                   return new string[] { };
               }))
               .ForMember(x => x.Options, m => m.MapFrom(c => c.Links));
        }
    }
}
