﻿using Litium.Web.Models;
using System.Collections.Generic;

namespace Litium.Accelerator.ViewModels.Framework
{
    public class FooterViewModel : PageViewModel
    {
        public virtual List<SectionModel> SectionList { get; set; } = new List<SectionModel>();
    }

    public class SectionModel
    {
        public virtual IList<LinkModel> SectionLinkList { get; set; } = new List<LinkModel>();
        public virtual EditorString SectionText { get; set; }
        public virtual string SectionTitle { get; set; }
    }
}