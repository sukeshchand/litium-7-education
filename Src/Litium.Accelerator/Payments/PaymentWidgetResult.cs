﻿using System;

namespace Litium.Accelerator.Payments
{
    public class PaymentWidgetResult
    {
        public bool IsChangeable { get; set; } = true;
        public string ResponseString { get; set; }
        public Uri RedirectUrl { get; set; }
        public string Id { get; set; }
    }
}
