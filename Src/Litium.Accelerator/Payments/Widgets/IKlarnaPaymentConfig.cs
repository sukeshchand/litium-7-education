﻿using Klarna.Rest.Models;
using Litium.AddOns.Klarna.Abstractions;
using Litium.AddOns.Klarna.PaymentArgs;
using Litium.Foundation.Modules.ECommerce.Carriers;
using Litium.Runtime.DependencyInjection;

namespace Litium.Accelerator.Payments.Widgets
{
    [Service(ServiceType = typeof(IKlarnaPaymentConfig))]
    public interface IKlarnaPaymentConfig
    {
        void UpdateDataSentToKlarna(OrderCarrier orderCarrier, KlarnaPaymentArgs paymentArgs, CheckoutOrderData klarnaCheckoutOrder);
        void AddressUpdate(CheckoutOrderData checkoutOrderData);
        ValidationResult ValidateCheckoutOrder(ILitiumKcoOrder order);
    }
}
