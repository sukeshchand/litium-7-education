﻿using Litium.Accelerator.Constants;
using Litium.Accelerator.Routing;
using Litium.Accelerator.Services.Payments;
using Litium.AddOns.Klarna;
using Litium.AddOns.Klarna.Abstractions;
using Litium.AddOns.Klarna.Configuration;
using Litium.AddOns.Klarna.Kco;
using Litium.AddOns.Klarna.PaymentArgs;
using Litium.FieldFramework.FieldTypes;
using Litium.Foundation.Modules.ECommerce.Carriers;
using Litium.Foundation.Modules.ECommerce.Plugins.Payments;
using Litium.Foundation.Modules.ECommerce.ShoppingCarts;
using Litium.Foundation.Security;
using Litium.Web;
using Litium.Web.Models.Websites;
using Litium.Web.Routing;
using Litium.Websites;
using System;
using System.Web;
using System.Web.Mvc;
using Litium.Runtime.AutoMapper;

namespace Litium.Accelerator.Payments.Widgets
{
    public class KlarnaWidget : IPaymentWidget<KlarnaProvider>
    {
        private Cart Cart => _requestModelAccessor.RequestModel.Cart;

        private readonly IPaymentInfoCalculator _paymentInfoCalculator;
        private readonly IKlarnaPaymentConfig _paymentWidgetConfigV3;
        private readonly SecurityToken _securityToken;
        private readonly RequestModelAccessor _requestModelAccessor;
        private readonly RouteRequestLookupInfoAccessor _routeRequestLookupInfoAccessor;
        private readonly UrlService _urlService;
        private readonly KlarnaPaymentService _klarnaPaymentService;
        private readonly PageService _pageService;

        public KlarnaWidget(
            IPaymentInfoCalculator paymentInfoCalculator,
            IKlarnaPaymentConfig paymentWidgetConfigV3,
            SecurityToken securityToken,
            RequestModelAccessor requestModelAccessor,
            RouteRequestLookupInfoAccessor routeRequestLookupInfoAccessor,
            UrlService urlService,
            KlarnaPaymentService klarnaPaymentService,
            PageService pageService)
        {
            _paymentInfoCalculator = paymentInfoCalculator;
            _paymentWidgetConfigV3 = paymentWidgetConfigV3;
            _securityToken = securityToken;
            _requestModelAccessor = requestModelAccessor;
            _routeRequestLookupInfoAccessor = routeRequestLookupInfoAccessor;
            _klarnaPaymentService = klarnaPaymentService;
            _urlService = urlService;
            _pageService = pageService;
        }

        public PaymentWidgetResult GetWidget(OrderCarrier order, PaymentInfoCarrier paymentInfo)
        {
            var paymentAccountId = _klarnaPaymentService.GetPaymentAccountId(paymentInfo.PaymentMethod);
            var klarnaCheckout = LitiumKcoApi.CreateFrom(paymentAccountId, _paymentWidgetConfigV3.UpdateDataSentToKlarna);

            var checkoutOrder = string.IsNullOrEmpty(paymentInfo.TransactionNumber) ? null : klarnaCheckout.FetchKcoOrder(order);
            if (checkoutOrder == null || checkoutOrder.KlarnaOrderStatus == KlarnaOrderStatus.Incomplete)
            {
                using (new KlarnaPaymentService.DistributedLock(order))
                {
                    _paymentInfoCalculator.CalculateFromCarrier(order, _securityToken);
                    var args = CreatePaymentArgs(order, paymentAccountId);

                    checkoutOrder = klarnaCheckout.CreateOrUpdateKcoOrder(order, args);
                }
            }

            switch (checkoutOrder.KlarnaOrderStatus)
            {
                case KlarnaOrderStatus.Incomplete:
                    return new PaymentWidgetResult
                    {
                        Id = nameof(PaymentMethod.KlarnaCheckout),
                        IsChangeable = true,
                        ResponseString = checkoutOrder.HtmlSnippet,
                    };
                case KlarnaOrderStatus.Error:
                    throw new Exception(checkoutOrder.HtmlSnippet);
                case KlarnaOrderStatus.Authorized:
                case KlarnaOrderStatus.Cancelled:
                case KlarnaOrderStatus.Captured:
                case KlarnaOrderStatus.Complete:
                case KlarnaOrderStatus.Created:
                    Cart.Clear();
                    return new PaymentWidgetResult
                    {
                        Id = nameof(PaymentMethod.KlarnaCheckout),
                        IsChangeable = false,
                        ResponseString = checkoutOrder.HtmlSnippet,
                    };
            }

            throw new Exception(checkoutOrder.HtmlSnippet);
        }

        bool IPaymentWidget.IsEnabled(string paymentMethod)
        {
            return paymentMethod.EndsWith(nameof(PaymentMethod.KlarnaCheckout), StringComparison.OrdinalIgnoreCase);
        }

        private ExecutePaymentArgs CreatePaymentArgs(OrderCarrier order, string paymentAccountId)
        {
            if (!_routeRequestLookupInfoAccessor.RouteRequestLookupInfo.IsSecureConnection)
            {
                this.Log().Trace("Klarna Checkout Validation is disabled. To enable the validate you need to use https on the checkout page.");
            }

            var checkoutFlowInfo = Cart.CheckoutFlowInfo;
            checkoutFlowInfo.ExecutePaymentMode = ExecutePaymentMode.Reserve;
            checkoutFlowInfo.RequireConsumerConfirm = false;

            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            var checkoutPage = _requestModelAccessor.RequestModel.WebsiteModel.Fields.GetValue<PointerPageItem>(AcceleratorWebsiteFieldNameConstants.CheckouPage);
            var checkoutPageUrl = GetAbsolutePageUrl(checkoutPage);
            var checkoutPageEntity = checkoutPage.EntitySystemId.MapTo<PageModel>();
            var termsAndConditionPage = checkoutPageEntity.GetValue<PointerPageItem>(CheckoutPageFieldNameConstants.TermsAndConditionsPage);
            var termsAndConditionPageUrl = GetAbsolutePageUrl(termsAndConditionPage);

            var channelUri = new Uri(_urlService.GetUrl(_requestModelAccessor.RequestModel.ChannelModel.Channel, new ChannelUrlArgs() { AbsoluteUrl = true }));
            var confirmationUrl = new Uri(channelUri, urlHelper.Action("Confirmation", "KlarnaPayment", null, Uri.UriSchemeHttps));
            var validateUrl = new Uri(channelUri, urlHelper.Action("Validate", "KlarnaPayment", null, Uri.UriSchemeHttps));
            var pushUrl = new Uri(channelUri, urlHelper.Action("PushNotification", "KlarnaPayment", null, Uri.UriSchemeHttps));
            var updateAddressUrl = new Uri(channelUri, urlHelper.Action("AddressUpdate", "KlarnaPayment", null, Uri.UriSchemeHttps));

            checkoutFlowInfo.SetValue(ConstantsAndKeys.TermsUrlKey, termsAndConditionPageUrl);
            checkoutFlowInfo.SetValue(ConstantsAndKeys.CheckoutUrlKey, checkoutPageUrl);
            checkoutFlowInfo.SetValue(ConstantsAndKeys.ConfirmationUrlKey, confirmationUrl.AbsoluteUri);
            checkoutFlowInfo.SetValue(ConstantsAndKeys.PushUrlKey, pushUrl.AbsoluteUri);

            checkoutFlowInfo.SetValue(ConstantsAndKeys.ValidationUrlKey, validateUrl.AbsoluteUri);
            checkoutFlowInfo.SetValue(ConstantsAndKeys.AddressUpdateUrlKey, updateAddressUrl.AbsoluteUri);

            return new KlarnaPaymentArgsCreator().CreatePaymentArgs(checkoutFlowInfo);
        }

        private string GetAbsolutePageUrl(PointerPageItem pointer)
        {
            if (pointer == null)
            {
                return null;
            }
            var page = _pageService.Get(pointer.EntitySystemId);
            if (page == null)
            {
                return null;
            }

            var channelSystemId = pointer.ChannelSystemId != Guid.Empty ? pointer.ChannelSystemId : _routeRequestLookupInfoAccessor.RouteRequestLookupInfo.Channel.SystemId;
            return _urlService.GetUrl(page, new PageUrlArgs(channelSystemId) { AbsoluteUrl = true });
        }
    }
}
