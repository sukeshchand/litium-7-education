﻿namespace Litium.Accelerator.Constants
{
    internal static class CheckoutPageFieldNameConstants
    {
        public const string TermsAndConditionsStatement = "TermsAndConditionsStatement";
        public const string TermsAndConditionsLinkText = "TermsAndConditionsLinkText";
        public const string TermsAndConditionsPage = "TermsAndConditionsLinkPage";
    }
}
