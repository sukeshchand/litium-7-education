﻿namespace Litium.Accelerator.Constants
{
    public static class AcceleratorWebsiteFieldNameConstants
    {
        public const string LogotypeMain = "LogotypeMain";
        public const string LogotypeIcon = "LogotypeIcon";
        public const string HeaderLayout = "HeaderLayout";
        public const string CheckouPage = "CheckouPage";
        public const string MyPagesPage = "MyPagesPage";
        public const string AdditionalHeaderLinks = "AdditionalHeaderLinks";
        public const string SearchResultPage = "SearchResultPage";
        public const string FooterHeader1 = "FooterHeader1";
        public const string FooterLinkList1 = "FooterLinkList1";
        public const string FooterText1 = "FooterText1";
        public const string FooterHeader2 = "FooterHeader2";
        public const string FooterLinkList2 = "FooterLinkList2";
        public const string FooterText2 = "FooterText2";
        public const string FooterHeader3 = "FooterHeader3";
        public const string FooterLinkList3 = "FooterLinkList3";
        public const string FooterText3 = "FooterText3";
        public const string FooterHeader4 = "FooterHeader4";
        public const string FooterLinkList4 = "FooterLinkList4";
        public const string FooterText4 = "FooterText4";
        public const string NavigationTheme = "NavigationTheme";
        public const string InFirstLevelCategories = "InFirstLevelCategories";
        public const string InBrandPages = "InBrandPages";
        public const string InProductListPages = "InProductListPages";
        public const string InArticlePages = "InArticlePages";
        public const string ProductsPerPage = "ProductsPerPage";
        public const string ShowBuyButton = "ShowBuyButton";
        public const string ShowQuantityFieldProductList = "ShowQuantityFieldProductList";
        public const string ShowQuantityFieldProductPage = "ShowQuantityFieldProductPage";
        public const string FiltersOrdering = "FiltersOrdering";
        public const string FiltersIndexedBySearchEngines = "FiltersIndexedBySearchEngines";
        public const string CheckoutMode = "CheckoutMode";
        public const string AllowCustomersEditLogin = "AllowCustomersEditLogin";
        public const string SenderEmailAddress = "SenderEmailAddress";
        public const string OrderConfirmationPage = "OrderConfirmationPage";
    }
}
