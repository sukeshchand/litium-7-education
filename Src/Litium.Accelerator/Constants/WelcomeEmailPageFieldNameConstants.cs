﻿namespace Litium.Accelerator.Constants
{
    internal static class WelcomeEmailPageFieldNameConstants
    {
        public const string EmailText = "EmailText";
        public const string Subject = "WelcomeEmailSubject";
    }
}
