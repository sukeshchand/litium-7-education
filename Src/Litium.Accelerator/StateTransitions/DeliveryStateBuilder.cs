﻿using Litium.Accelerator.Utilities;
using Litium.Foundation.Modules.ECommerce;
using Litium.Foundation.Modules.ECommerce.Carriers;
using Litium.Foundation.Modules.ECommerce.StateTransitionSystem;

namespace Litium.Accelerator.StateTransitions
{
    /// <summary>
    ///     Builds delivery states.
    /// </summary>
    public class DeliveryStateBuilder
    {
        /// <summary>
        ///     Builds the delivery states.
        /// </summary>
        /// <param name="stateMachine">The state machine.</param>
        public virtual void Build(DeliveryStateMachine stateMachine)
        {
            //Processing: Delivery has started processing.
            var processing = new State<DeliveryCarrier>((short)DeliveryState.Processing,
                DeliveryState.Processing.ToString(),
                (deliveryCarrier, currentState, token) =>
                {
                    //Delivery processing entry action.

                    //TODO: Integration code to execute, when start processing the delivery.
                    //...
                },
                null);

            //Delivered: Delivery is completed, usually this state means that delivery is sent from merchants warehouse to the distributor.
            var delivered = new State<DeliveryCarrier>((short)DeliveryState.Delivered,
                DeliveryState.Delivered.ToString(),
                (deliveryCarrier, currentState, token) =>
                {
                    //Delivery processing entry action.

                    //TODO: Integration code to execute, when delivery is sent out from warehouse.
                    //...

                    //Collect the payment, if the payment is still Reserved.
                    OrderUtilities.CompletePayments(ModuleECommerce.Instance.Orders[deliveryCarrier.OrderID, token], token);
                },
                null);

            //Cancelled: Delivery is cancelled, e.g. requested by end customer, or cancelled due to delivery provider failed to deliver the item.
            var cancelled = new State<DeliveryCarrier>((short)DeliveryState.Cancelled,
                DeliveryState.Cancelled.ToString(),
                (deliveryCarrier, currentState, token) =>
                {
                    //Delivery cancellation entry action.

                    //TODO: Integration code to execute, when the delivery is cancelled.
                    //...

                    //TODO: Send delivery cancellation email.
                    //...

                    //Return all the payments, if they are already Paid, or Cancel them if they are still reserved.
                    OrderUtilities.ReturnOrCancelAllPayments(ModuleECommerce.Instance.Orders[deliveryCarrier.OrderID, token], token);
                },
                null);

            //Failed: Delivery failed, e.g. delivery provider failed to delivery the item to delivery address specified.
            var failed = new State<DeliveryCarrier>((short)DeliveryState.Failed,
                DeliveryState.Failed.ToString(),
                (deliveryCarrier, currentState, token) =>
                {
                    //Delivery failure entry action.

                    //TODO: Integration code to execute, when the delivery was sent to distributor, but then it failed.
                    //...

                    //TODO: Send delivery failure notice email.
                    //...
                },
                null);

            //Returned: Delivery is returned by the end customer.
            var returned = new State<DeliveryCarrier>((short)DeliveryState.Returned,
                DeliveryState.Returned.ToString(),
                (deliveryCarrier, currentState, token) =>
                {
                    //Delivery returned entry action.

                    //TODO: Integration code to execute, when the delivery was returned.
                    //...

                    //TODO: Send delivery return notice email.
                    //...
                },
                null);

            //add state transitions.
            //from Init to Processing.
            stateMachine.AddStateTransition(new State<DeliveryCarrier>((short)DeliveryState.Init, DeliveryState.Init.ToString()),
                processing,
                (deliveryCarrier, startState, endState, token) =>
                {
                    //Condition for delivery to go from Init to Processing.

                    //Delivery needs a confirmed order to start processing.
                    var order = ModuleECommerce.Instance.Orders[deliveryCarrier.OrderID, token];
                    return order != null && order.OrderStatus == (short)OrderState.Confirmed;
                },
                true);

            //from init to cancelled.
            stateMachine.AddStateTransition(new State<DeliveryCarrier>((short)DeliveryState.Init, DeliveryState.Init.ToString()), cancelled);

            //from Processing to Delivered.
            stateMachine.AddStateTransition(processing, delivered);
            //from delivered to returned.
            stateMachine.AddStateTransition(delivered, returned);
            //from delivered to failed.
            stateMachine.AddStateTransition(delivered, failed);
            //from failed to delivered.
            stateMachine.AddStateTransition(failed, delivered);
            //from failed to cancelled
            stateMachine.AddStateTransition(failed, cancelled);
        }
    }
}