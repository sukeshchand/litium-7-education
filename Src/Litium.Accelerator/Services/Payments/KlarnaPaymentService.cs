﻿using JetBrains.Annotations;
using Litium.Accelerator.Payments.Widgets;
using Litium.Accelerator.ViewModels.Checkout;
using Litium.AddOns.Klarna.Abstractions;
using Litium.AddOns.Klarna.Kco;
using Litium.Foundation.Modules.ECommerce;
using Litium.Foundation.Modules.ECommerce.Carriers;
using Litium.Foundation.Modules.ECommerce.Orders;
using Litium.Foundation.Modules.ECommerce.Payments;
using Litium.Foundation.Modules.ECommerce.ShoppingCarts;
using Litium.Foundation.Security;
using Litium.Runtime.DependencyInjection;
using Litium.Web.Customers.TargetGroups;
using Litium.Web.Customers.TargetGroups.Events;
using System;
using System.Linq;
using System.Threading;

namespace Litium.Accelerator.Services.Payments
{
    [Service(ServiceType = typeof(KlarnaPaymentService))]
    public class KlarnaPaymentService
    {
        private readonly CartAccessor _cartAccessor;
        private readonly TargetGroupEngine _targetGroupEngine;
        private readonly ModuleECommerce _moduleECommerce;
        private readonly SecurityToken _securityToken;
        private readonly IKlarnaPaymentConfig _paymentWidgetConfig;

        public KlarnaPaymentService(IKlarnaPaymentConfig paymentWidgetConfig,  SecurityToken securityToken, 
            ModuleECommerce moduleECommerce, TargetGroupEngine targetGroupEngine, CartAccessor cartAccessor)
        {
            _cartAccessor = cartAccessor;
            _paymentWidgetConfig = paymentWidgetConfig;
            _securityToken = securityToken;
            _targetGroupEngine = targetGroupEngine;
            _moduleECommerce = moduleECommerce;
        }

        public ILitiumKcoApi CreateKlarnaCheckoutApi(string merchantId)
        {
            var klarnaCheckout = LitiumKcoApi.CreateFrom(merchantId, _paymentWidgetConfig.UpdateDataSentToKlarna);
            return klarnaCheckout;
        }

        public string GetPaymentAccountId(string paymentMethod)
        {
            var paymentMethodParts = paymentMethod.Split(' ');
            if (paymentMethodParts.Length > 0)
            {
                return paymentMethodParts[0];
            }

            return null;
        }

        public void PlaceOrder([NotNull] ILitiumKcoApi klarnaCheckout, [NotNull] ILitiumKcoOrder checkoutOrder, bool isConfirmation)
        {
            this.Log().Debug("Klarna order status klarna order status {klarnaOrderStatus}.", checkoutOrder.KlarnaOrderStatus);
            Order order = null;
            OrderCarrier orderCarrier = null;
            if (checkoutOrder?.KlarnaOrderStatus == KlarnaOrderStatus.Complete)
            {
                using (new DistributedLock(checkoutOrder))
                {
                    //there is a time gap between when we last fetched order from Klarna to the time of aquiring the distributed lock.
                    //just before the distributed lock was taken by this machine (but after this machine had fetched checkout order from klarna),
                    //another machine in the server farm may have got the lock, and created the order and exited releasing the distributed lock.
                    //That would cause this machine to aquire the lock, but we cannot use the existing checkoutorder because its out-dated.
                    //therefore we have to refetch it one more time!.
                    checkoutOrder = klarnaCheckout.FetchKcoOrder(checkoutOrder.OrderCarrier);

                    if (checkoutOrder?.KlarnaOrderStatus == KlarnaOrderStatus.Complete)
                    {
                        //save the order only if it is not saved already. 
                        order = TryFindOrder();
                        if (order == null)
                        {
                            this.Log().Debug("Create order.");
                            // Replace the order carrier in the session from the one received from Klarna plugin.
                            // The order carrier contains the inforamtion of the order that will be created.
                            _cartAccessor.Cart.OrderCarrier = checkoutOrder.OrderCarrier;

                            order = _cartAccessor.Cart.PlaceOrder(_securityToken, out var paymentInfos);
                            this.Log().Debug("Order created, order id {orderId}, external order id {externalOrderId}.", order.ID, order.ExternalOrderID);
                        }

                        var paymentInfo = order.PaymentInfo.FirstOrDefault(x => x.PaymentProviderName == klarnaCheckout.PaymentProviderName);
                        if (paymentInfo != null)
                        {
                            // If the Litium call to Klarna to "notify order created" fails, the payment will be in following states.
                            // so execute payment need to be called again, to notify klarna of Litium order id.
                            if (paymentInfo.PaymentStatus == PaymentStatus.Init
                                || paymentInfo.PaymentStatus == PaymentStatus.ExecuteReserve
                                || paymentInfo.PaymentStatus == PaymentStatus.Pending)
                            {
                                this.Log().Debug("Execute payment.");
                                paymentInfo.ExecutePayment(_cartAccessor.Cart.CheckoutFlowInfo, _securityToken);
                                this.Log().Debug("Payment executed.");

                                // The order carrier has changed, refetch the checkoutOrder with updated data.
                                orderCarrier = order.GetAsCarrier(true, true, true, true, true, true);
                                checkoutOrder = klarnaCheckout.FetchKcoOrder(orderCarrier);
                                this.Log().Debug("Klarna order status changed to {klarnaOrderStatus}.", checkoutOrder.KlarnaOrderStatus);
                            }
                        }
                    }
                }
            }

            if (isConfirmation && checkoutOrder?.KlarnaOrderStatus == KlarnaOrderStatus.Created)
            {
                var placedOrder = order ?? TryFindOrder();
                if (placedOrder != null)
                {
                    _cartAccessor.Cart.OrderCarrier = orderCarrier ?? placedOrder.GetAsCarrier(true, true, true, true, true, true);
                    if (order == null)
                    {
                        this.Log().Debug("Processing target group event for external order id {externalOrderId}.", placedOrder.ExternalOrderID);
                        _targetGroupEngine.Process(new OrderEvent { Order = placedOrder });
                    }
                    else
                    {
                        this.Log().Debug("Target group processing for external order id {externalOrderId} was executed with the order creation.", placedOrder.ExternalOrderID);
                    }
                }
            }

            Order TryFindOrder()
            {
                var checkoutOrderCarrier = checkoutOrder.OrderCarrier;
                var hasExternalOrderId = !string.IsNullOrEmpty(checkoutOrderCarrier.ExternalOrderID);
                var foundOrder = hasExternalOrderId
                    ? _moduleECommerce.Orders.GetOrder(checkoutOrderCarrier.ExternalOrderID, _securityToken)
                    : _moduleECommerce.Orders.GetOrder(checkoutOrderCarrier.ID, _securityToken);

                if (foundOrder == null)
                {
                    if (hasExternalOrderId)
                    {
                        this.Log().Debug("No order exists for external order external id {externalOrderId}.", checkoutOrderCarrier.ExternalOrderID);
                    }
                    else
                    {
                        this.Log().Debug("No order exists for order id {orderId}.", checkoutOrderCarrier.ID);
                    }

                    var transactionNumber = checkoutOrderCarrier.PaymentInfo.FirstOrDefault()?.TransactionNumber;
                    if (!string.IsNullOrEmpty(transactionNumber))
                    {
                        this.Log().Debug("Try to find order based on transactionNumber {transactionNumber}.", transactionNumber);
                        foundOrder = _moduleECommerce.Orders.GetOrdersByTransactionNumber(klarnaCheckout.PaymentProviderName, transactionNumber, _securityToken).FirstOrDefault();
                        if (foundOrder == null)
                        {
                            this.Log().Debug("No order based on transactionNumber {transactionNumber} was found.", transactionNumber);
                        }
                    }
                    else
                    {
                        this.Log().Debug("No transaction number exists on information from Klarna, no order was found.");
                    }
                }

                if (foundOrder != null)
                {
                    this.Log().Debug("Persisted order is found, order id {orderId}, external order id {externalOrderId}.", foundOrder.ID, foundOrder.ExternalOrderID);
                }
                return foundOrder;
            }
        }

        public void SetAddress(KlarnaOnChangeViewModel args, AddressCarrier addressCarrier)
        {
            if (addressCarrier != null)
            {
                addressCarrier.FirstName = args.GivenName;
                addressCarrier.LastName = args.FamilyName;
                addressCarrier.Email = args.Email;
                addressCarrier.Country = args.Country;
                addressCarrier.Zip = args.PostalCode;
            }
        }

        public class DistributedLock : IDisposable
        {
            private readonly string _syncLock;
            private readonly Guid _token;

            public DistributedLock(OrderCarrier order)
            {
                _syncLock = order.ID.ToString();
                _token = TakeLock();
            }

            public DistributedLock(ILitiumKcoOrder checkoutOrder)
            {
                _syncLock = checkoutOrder.KlarnaId;
                _token = TakeLock();
            }

            public void Dispose()
            {
                DistributedSyncLock.Exit(_syncLock, _token);
            }

            private Guid TakeLock()
            {
                Guid item;
                while (!DistributedSyncLock.TryEnter(_syncLock, out item))
                {
                    Thread.Sleep(50);
                }

                return item;
            }
        }
    }
}
