﻿using System;
using System.Collections.Generic;
using Litium.Foundation.Modules.ECommerce.Carriers;
using Litium.Foundation.Modules.ECommerce.Orders;
using Litium.Foundation.Modules.ECommerce.Payments;
using Litium.Runtime.DependencyInjection;

namespace Litium.Accelerator.Services
{
    [Litium.Runtime.DependencyInjection.Service(
        ServiceType = typeof(IAuthorService),
        Lifetime = Litium.Runtime.DependencyInjection.DependencyLifetime.Transient)]
    public interface IAuthorService
    {
        List<string> GetBooksByAuthor(Guid authorPageId);
        List<Tuple<string, Guid>> GetAuthors();
    }
}
