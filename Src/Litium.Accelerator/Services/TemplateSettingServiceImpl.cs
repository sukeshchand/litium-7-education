﻿using System;
using System.Collections.Concurrent;
using Litium.Foundation.Settings;

namespace Litium.Accelerator.Services
{
    internal class TemplateSettingServiceImpl : TemplateSettingService
    {
        private readonly ConcurrentDictionary<string, string> _keyResolver = new ConcurrentDictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        private readonly ISettingService _settingService;

        public TemplateSettingServiceImpl(ISettingService settingService)
        {
            _settingService = settingService;
        }

        public override string GetTemplateGroupingField(string templateId)
        {
            return _settingService.Get<string>(_keyResolver.GetOrAdd(templateId, GetKey));
        }

        public override void SetTemplateGroupings(string templateId, string groupingField)
        {
            _settingService.Set(_keyResolver.GetOrAdd(templateId, GetKey), groupingField);
        }

        private string GetKey(string id)
        {
            return $"IndexingTemplateGrouping:{id}";
        }
    }
}
