﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Data;
using Litium.Data.Queryable;
using Litium.FieldFramework;
using Litium.Foundation;
using Litium.Foundation.Modules.CMS.Search;
using Litium.Foundation.Search;
using Litium.Framework.Search;
using Litium.Products;
using Litium.Websites;

namespace Litium.Accelerator.Services
{
    //public class AuthorService : IAuthorService
    //{
    //    public List<string> GetBooksByAuthor(Guid authorPageId)
    //    {
    //        return new List<string>()
    //        {
    //            "Oru deshathinde kadha",
    //            "Kayar",
    //            "Ummachi"
    //        };
    //    }
    //}
    public class AuthorService : IAuthorService
    {
        private readonly DataService _dataService;
        private readonly Solution _solution;
        private readonly FieldTemplateService _fieldTemplateService;
        private readonly PageService _pageService;

        public AuthorService(PageService pageService, DataService dataService, Litium.Foundation.Solution solution, Litium.FieldFramework.FieldTemplateService fieldTemplateService)
        {
            _pageService = pageService;
            _dataService = dataService;
            _solution = solution;
            _fieldTemplateService = fieldTemplateService;
        }

        public List<Tuple<string, Guid>> GetAuthors()
        {
            var authorPageTemplate = _fieldTemplateService.Get<FieldTemplate>(typeof(WebsiteArea), "Author");

            var request = new QueryRequest(_solution.Languages.DefaultLanguageID, CmsSearchDomains.Pages, _solution.SystemToken);
            request.FilterTags.Add(new Tag(TagNames.TemplateId, authorPageTemplate.Id));
            var searchResult = _solution.SearchService.Search(request);

            if (searchResult.Hits.Count == 0)
                throw new Exception("No authors found");

            var result = new List<Tuple<string, Guid>>();
            foreach (var hit in searchResult.Hits)
            {
                var authorPage = _pageService.Get(new Guid(hit.Id));
                result.Add(new Tuple<string, Guid>(authorPage.Localizations.CurrentUICulture.Name, authorPage.SystemId));
            }

            return result;
        }

        public List<string> GetBooksByAuthor(Guid authorPageId)
        {
            var authorPage = _pageService.Get(authorPageId);
            if (authorPage == null)
                throw new Exception($"Page not found for author with id {authorPageId}");

            using (var query = _dataService.CreateQuery<BaseProduct>())
            {
                var bookQuery = query.Filter(filter => filter
                    .Bool(boolFilter => boolFilter
                        .Must(boolFilterMust => boolFilterMust
                            .Field("Author", "eq", authorPage.SystemId))));

                var books = bookQuery.ToList();
                var bookTitles = books.Select(book => book.Localizations.CurrentCulture.Name).ToList();
                return bookTitles;
            }
        }
    }


}
