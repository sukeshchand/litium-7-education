﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Litium.Accelerator.Services
{
    [Litium.Runtime.DependencyInjection.ServiceDecorator(typeof(IAuthorService))]
    public class AuthorServiceRatingsDecorator : IAuthorService
    {
        private readonly IAuthorService _authorService;

        public AuthorServiceRatingsDecorator(IAuthorService authorService)
        {
            _authorService = authorService;
        }
        public List<string> GetBooksByAuthor(Guid authorPageId)
        {
            return _authorService.GetBooksByAuthor(authorPageId).Select(x => $"{x} (Rating*: {x?.Length ?? 0})").ToList();
        }

        public List<Tuple<string, Guid>> GetAuthors()
        {
            return null;
        }
    }
}