﻿using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.ProductCatalog;
using Litium.Products.StockStatusCalculator;
using Litium.Studio.Extenssions;

namespace Litium.Accelerator.Services
{
    internal class StockStatusMessageServiceImpl : IStockStatusMessageService
    {
        public StockStatusCalculatorResult Populate(StockStatusCalculatorArgs calculatorArgs, StockStatusCalculatorItemArgs calculatorItemArgs, StockStatusCalculatorResult result)
        {
            var key = (calculatorItemArgs.Quantity == 0 ? result.InStockQuantity > 0 : result.InStockQuantity >= calculatorItemArgs.Quantity) ? "stock.instockwithoutquantity" : "stock.outofstockwithoutquantity";
            result.Description = string.Format(key.AsWebSiteString() ?? key.AsModuleString<ModuleProductCatalog>() ?? "{0}", result.InStockQuantity);
            return result;
        }
    }
}
