﻿using Litium.Runtime.DependencyInjection;

namespace Litium.Accelerator.Services
{
    [Service(ServiceType = typeof(TemplateSettingService))]
    public abstract class TemplateSettingService
    {
        public abstract string GetTemplateGroupingField(string templateId);
        public abstract void SetTemplateGroupings(string templateId, string groupingField);
    }
}
