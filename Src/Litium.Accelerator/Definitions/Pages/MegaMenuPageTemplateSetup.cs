﻿using Litium.Websites;
using Litium.FieldFramework;
using System.Collections.Generic;
using Litium.Accelerator.Constants;

namespace Litium.Accelerator.Definitions.Pages
{
    internal class MegaMenuPageTemplateSetup : FieldTemplateSetup
    {
        public override IEnumerable<FieldTemplate> GetTemplates()
        {
            var template = new PageFieldTemplate(PageTemplateNameConstants.MegaMenu)
            {
                FieldGroups = new List<FieldTemplateFieldGroup>
                {
                    new FieldTemplateFieldGroup()
                    {
                        Id = "General",
                        Collapsed = false,
                        Fields =
                        {
                            SystemFieldDefinitionConstants.Name,
                            SystemFieldDefinitionConstants.Url
                        }
                    },
                    new FieldTemplateFieldGroup()
                    {
                        Id = "Link",
                        Collapsed = false,
                        Fields =
                        {
                            MegaMenuPageFieldNameConstants.MegaMenuCategory,
                            MegaMenuPageFieldNameConstants.MegaMenuPage
                        }
                    },
                    new FieldTemplateFieldGroup()
                    {
                        Id = "MegaMenu",
                        Collapsed = false,
                        Fields =
                        {
                            MegaMenuPageFieldNameConstants.MegaMenuShowContent,
                            MegaMenuPageFieldNameConstants.MegaMenuShowSubCategories
                        }
                    }
                }
            };
            for (int i = 1; i <= 4; i++)
            {
                template.FieldGroups.Add(new FieldTemplateFieldGroup()
                {
                    Id = "MegaMenuColumn" + i,
                    Collapsed = false,
                    Fields =
                    {
                        MegaMenuPageFieldNameConstants.MegaMenuColumnHeader + i,
                        MegaMenuPageFieldNameConstants.MegaMenuCategories + i,
                        MegaMenuPageFieldNameConstants.MegaMenuPages + i,
                        MegaMenuPageFieldNameConstants.MegaMenuFilters + i,
                        MegaMenuPageFieldNameConstants.MegaMenuEditor + i,
                        MegaMenuPageFieldNameConstants.MegaMenuAdditionalLink + i,
                        MegaMenuPageFieldNameConstants.MegaMenuLinkToCategory + i,
                        MegaMenuPageFieldNameConstants.MegaMenuLinkToPage + i
                    }
                });
            }

            return new List<FieldTemplate>() { template };
        }
    }
}
