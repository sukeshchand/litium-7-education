﻿using Litium.Accelerator.Constants;
using Litium.FieldFramework;
using Litium.FieldFramework.FieldTypes;
using Litium.Websites;
using System.Collections.Generic;

namespace Litium.Accelerator.Definitions.Pages
{
    internal class PageFieldDefinitionSetup : FieldDefinitionSetup
    {
        public override IEnumerable<FieldDefinition> GetFieldDefinitions()
        {
            var fields = new List<FieldDefinition>();

            fields.AddRange(GeneralFields());
            fields.AddRange(LoginPageFields());
            fields.AddRange(MegaMenuPageFields());

            return fields;
        }

        private IEnumerable<FieldDefinition> GeneralFields()
        {
            var fields = new List<FieldDefinition>
            {
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.Title, SystemFieldTypeConstants.Text)
                {
                    MultiCulture = true,
                },
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.Introduction, SystemFieldTypeConstants.MultirowText)
                {
                    MultiCulture = true,
                },
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.Text, SystemFieldTypeConstants.Editor)
                {
                    MultiCulture = true,
                },
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.Links, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.WebsitesPage, MultiSelect = true }
                },
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.Image, SystemFieldTypeConstants.MediaPointerImage),
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.Files, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.MediaFile, MultiSelect = true }
                },
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.PageSize, SystemFieldTypeConstants.Int),
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.TitleFilterSelector, SystemFieldTypeConstants.Text),
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.ErrorMessage, SystemFieldTypeConstants.Editor)
                {
                    MultiCulture = true,
                },
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.NumberOfNewsPerPage, SystemFieldTypeConstants.Int),
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.NewsDate, SystemFieldTypeConstants.DateTime),
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.CategoryPointer, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.ProductsCategory, MultiSelect = false }
                },
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.ImageText, SystemFieldTypeConstants.Text),
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.AlternativeImageDescription, SystemFieldTypeConstants.Text),
                new FieldDefinition<WebsiteArea>(WelcomeEmailPageFieldNameConstants.Subject, SystemFieldTypeConstants.Text),
                new FieldDefinition<WebsiteArea>(WelcomeEmailPageFieldNameConstants.EmailText, SystemFieldTypeConstants.Editor),
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.NumberOfOrdersPerPage, SystemFieldTypeConstants.Int),
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.OrderLink, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.WebsitesPage, MultiSelect = false }
                },
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.OrderHistoryLink, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.WebsitesPage, MultiSelect = false }
                },
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.ProductSetPointer, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.ProductsProductList, MultiSelect = false }
                },
                new FieldDefinition<WebsiteArea>(PageFieldNameConstants.MayUserEditLogin, SystemFieldTypeConstants.Boolean)
            };
            return fields;
        }

        private IEnumerable<FieldDefinition> LoginPageFields()
        {
            var fields = new List<FieldDefinition>
            {
                new FieldDefinition<WebsiteArea>(LoginPageFieldNameConstants.ForgottenPasswordLink, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.WebsitesPage }
                },
                new FieldDefinition<WebsiteArea>(LoginPageFieldNameConstants.RedirectLink, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.WebsitesPage, MultiSelect = false }
                }
            };
            return fields;
        }

        private IEnumerable<FieldDefinition> MegaMenuPageFields()
        {
            var fields = new List<FieldDefinition<WebsiteArea>>
            {
                new FieldDefinition<WebsiteArea>(MegaMenuPageFieldNameConstants.MegaMenuCategory, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.ProductsCategory }
                },
                new FieldDefinition<WebsiteArea>(MegaMenuPageFieldNameConstants.MegaMenuPage, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.WebsitesPage }
                },
                new FieldDefinition<WebsiteArea>(MegaMenuPageFieldNameConstants.MegaMenuShowContent, SystemFieldTypeConstants.Boolean),
                new FieldDefinition<WebsiteArea>(MegaMenuPageFieldNameConstants.MegaMenuShowSubCategories, SystemFieldTypeConstants.Boolean)
            };
            for (int i = 1; i <= 4; i++)
            {
                fields.Add(new FieldDefinition<WebsiteArea>(MegaMenuPageFieldNameConstants.MegaMenuColumnHeader + i, SystemFieldTypeConstants.LimitedText));
                fields.Add(new FieldDefinition<WebsiteArea>(MegaMenuPageFieldNameConstants.MegaMenuCategories + i, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.ProductsCategory, MultiSelect = true }
                });
                fields.Add(new FieldDefinition<WebsiteArea>(MegaMenuPageFieldNameConstants.MegaMenuPages + i, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.WebsitesPage, MultiSelect = true }
                });
                fields.Add(new FieldDefinition<WebsiteArea>(MegaMenuPageFieldNameConstants.MegaMenuFilters + i, FieldTypeConstants.Filters));
                fields.Add(new FieldDefinition<WebsiteArea>(MegaMenuPageFieldNameConstants.MegaMenuEditor + i, SystemFieldTypeConstants.Editor));
                fields.Add(new FieldDefinition<WebsiteArea>(MegaMenuPageFieldNameConstants.MegaMenuAdditionalLink + i, SystemFieldTypeConstants.LimitedText));
                fields.Add(new FieldDefinition<WebsiteArea>(MegaMenuPageFieldNameConstants.MegaMenuLinkToCategory + i, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.ProductsCategory }
                });
                fields.Add(new FieldDefinition<WebsiteArea>(MegaMenuPageFieldNameConstants.MegaMenuLinkToPage + i, SystemFieldTypeConstants.Pointer)
                {
                    Option = new PointerOption { EntityType = PointerTypeConstants.WebsitesPage }
                });
            }

            return fields;
        }
    }
}
