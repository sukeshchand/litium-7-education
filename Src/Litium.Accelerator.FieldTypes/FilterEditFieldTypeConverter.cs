using System.Collections.Generic;
using Litium.Runtime.DependencyInjection;
using Litium.Web.Administration.FieldFramework;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Litium.Accelerator.FieldTypes
{
    [Service(Name = "FilterFields")]
    internal class FilterEditFieldTypeConverter : IEditFieldTypeConverter
    {
        private readonly JsonSerializer _jsonSerializer;

        public FilterEditFieldTypeConverter(JsonSerializer jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }

        public object ConvertFromEditValue(EditFieldTypeConverterArgs args, JToken item)
        {
            var array = item as JArray;
            List<string> items = null;

            if (array != null)
            {
                items = array.ToObject<List<string>>(_jsonSerializer);
            }

            var value = item as JValue;
            if (value != null)
            {
                items = new List<string>(new[] { value.ToObject<string>(_jsonSerializer) });
            }
            return items;
        }

        public JToken ConvertToEditValue(EditFieldTypeConverterArgs args, object item)
        {
            var items = item as IList<string> ?? new List<string>();
            return new JArray(items);
        }

        public object CreateOptionsModel() => null;
        public string EditControllerName { get; } = null;
        public string EditControllerTemplate { get; } = null;
        public string SettingsControllerName { get; }
        public string SettingsControllerTemplate { get; }

        /// <summary>
        /// The editor component name.
        /// </summary>
        /// <remark>
        /// Extension module should have module name prefix in order to be able to find the correct component at client side.
        /// </remark>
        public string EditComponentName => "Accelerator#FieldEditorFilterFields";

        /// <summary>
        /// The settings component name.
        /// </summary>
        /// <remark>
        /// Extension module should have module name prefix in order to be able to find the correct component at client side.
        /// </remark>
        public string SettingsComponentName { get; }
    }
}
