﻿using Litium.Accelerator.Mvc.Routing;
using Litium.Web.WebApi;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Web.Http;

namespace Litium.Accelerator.Mvc.App_Start
{
    public class WebApiConfig : IWebApiSetup
    {
        private static readonly string Route = "api";
        public static readonly string RoutePrefix = $"~/{Route}/";
        private readonly IServiceProvider _serviceProvider;

        public WebApiConfig(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void SetupWebApi(HttpConfiguration config)
        {
            // Convention-based routing            
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: Route + "/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            config.MessageHandlers.Add(ActivatorUtilities.CreateInstance<RequestModelHandler>(_serviceProvider));
        }
    }
}