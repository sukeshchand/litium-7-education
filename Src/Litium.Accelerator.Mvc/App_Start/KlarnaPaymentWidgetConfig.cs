﻿using Klarna.Rest.Models;
using Litium.Accelerator.Constants;
using Litium.Accelerator.Mvc.Controllers.Checkout;
using Litium.Accelerator.Payments.Widgets;
using Litium.Accelerator.Routing;
using Litium.AddOns.Klarna.Abstractions;
using Litium.AddOns.Klarna.Kco;
using Litium.AddOns.Klarna.PaymentArgs;
using Litium.FieldFramework.FieldTypes;
using Litium.Foundation.Modules.ECommerce.Carriers;
using Litium.Globalization;
using Litium.Runtime.AutoMapper;
using Litium.Web;
using Litium.Web.Models;
using Litium.Web.Mvc;
using Litium.Web.Routing;
using Litium.Websites;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Litium.Accelerator.Mvc.App_Start
{
    public class KlarnaPaymentWidgetConfig : IKlarnaPaymentConfig
    {
        private readonly IErrorPageResolver _errorPageResolver;
        private readonly WebsiteService _websiteService;
        private readonly ChannelService _channelService;
        private readonly LanguageService _languageService;
        private readonly UrlService _urlService;
        private readonly RouteRequestLookupInfoAccessor _routeRequestLookupInfoAccessor;
        private readonly RequestModelAccessor _requestModelAccessor;

        public KlarnaPaymentWidgetConfig(IErrorPageResolver errorPageResolver, 
            RouteRequestLookupInfoAccessor routeRequestLookupInfoAccessor,
            WebsiteService websiteService,
            ChannelService channelService,
            LanguageService languageService,
            UrlService urlService,
            RequestModelAccessor requestModelAccessor)
        {
            _errorPageResolver = errorPageResolver;
            _routeRequestLookupInfoAccessor = routeRequestLookupInfoAccessor;
            _requestModelAccessor = requestModelAccessor;
            _websiteService = websiteService;
            _channelService = channelService;
            _languageService = languageService;
            _urlService = urlService;
        }

        public void AddOrUpdateAdditionalOrderInfo(OrderCarrier orderCarrier, string key, string value)
        {
            var item = orderCarrier.AdditionalOrderInfo.FirstOrDefault(x => x.Key == key && !x.CarrierState.IsMarkedForDeleting);
            if (item == null)
            {
                orderCarrier.AdditionalOrderInfo.Add(new AdditionalOrderInfoCarrier(key, orderCarrier.ID, value));
            }
            else
            {
                if (item.Value != value)
                {
                    item.Value = value;
                }
            }
        }

        public void AddressUpdate(CheckoutOrderData checkoutOrderData)
        {
            // If the campaigns depend on address location, re-calculate the cart, after updating delivery addresses.
            // if you recalculate order totals, the checkoutOrderData must contain the correct order rows and order total values.
        }

        public void UpdateDataSentToKlarna(OrderCarrier orderCarrier, KlarnaPaymentArgs paymentArgs, CheckoutOrderData klarnaCheckoutOrder)
        {
            //if the project has specific data to be sent to Klarna, outside the order carrier,
            //or has them as additional order info, and is not handled by the Klarna addOn, modify the klarnaCheckoutOrder parameter.
            //it is the klarnaCheckoutOrder object that will be sent to Klarna checkout api at Klarna.
            //the format of klarnaCheckoutOrder parameter is described in Klarna API documentation https://developers.klarna.com.

            //Set the checkout options here.
            klarnaCheckoutOrder.Options = new CheckoutOptions
            {
                AllowSeparateShippingAddress = true,
                ColorButton = "#ff69b4",
                DateOfBirthMandatory = true
            };

            //External payment methods should be configured for each Merchant account by Klarna before they are used.
            AddCashOnDeliveryExternalPaymentMethod(klarnaCheckoutOrder);

            //to allow unrestricted shipping
        }

        /// <summary>
        ///     Validates the checkout order.
        ///     you may save additional order info into the order, during validations.
        ///     following is a sample which saves the date of birth as additional order info.
        /// </summary>
        /// <remarks>
        ///     The method need to return within 3 seconds to be able to cancel placed an order at Klarna.
        /// </remarks>
        /// <param name="order">The order.</param>
        /// <param name="controller">The controller.</param>
        /// <returns>ValidationResult.</returns>
        public ValidationResult ValidateCheckoutOrder(ILitiumKcoOrder order)
        {
            var kcoOrder = order as LitiumKcoOrder;
            if (!string.IsNullOrEmpty(kcoOrder?.CheckoutOrderData?.Customer?.DateOfBirth))
            {
                AddOrUpdateAdditionalOrderInfo(order.OrderCarrier, "DateOfBirth", kcoOrder.CheckoutOrderData.Customer.DateOfBirth);
            }

            var website = _websiteService.Get(order.OrderCarrier.WebSiteID);
            var channel = _channelService.Get(order.OrderCarrier.ChannelID);
            var language = channel.WebsiteLanguageSystemId.HasValue ? _languageService.Get(channel.WebsiteLanguageSystemId.Value) : null;
            CultureInfo.CurrentUICulture = language != null ? language.CultureInfo : CultureInfo.CurrentUICulture;
            var routeRequestLookupInfo = new RouteRequestLookupInfo()
            {
                Channel = channel,
                IsSecureConnection = true,
            };
            _errorPageResolver.TryGet(routeRequestLookupInfo, out var routeRequestInfo);

            // following result is used by Klarna AddOn to send the validation result back to Klarna.
            // ReSharper disable once ConvertToLambdaExpression
            return new ValidationResult
            {
                IsOrderValid = true,
                RedirectToUrlOnValidationFailure = routeRequestInfo.DataPath,
            };
        }

        /// <summary>
        ///     Adds the cash on delivery external payment method., by using Litium default "DirectPay" as the payment method.
        ///     Note: To use, Klarna has to configure the "Cash on delivery" external payment method for the merchant account.
        /// </summary>
        /// <param name="klarnaCheckoutOrder">The klarna checkout order.</param>
        private void AddCashOnDeliveryExternalPaymentMethod(CheckoutOrderData klarnaCheckoutOrder)
        {
            var checkoutPage = _requestModelAccessor.RequestModel.WebsiteModel.Fields.GetValue<PointerPageItem>(AcceleratorWebsiteFieldNameConstants.CheckouPage).EntitySystemId.MapTo<Page>();
            if (checkoutPage == null)
            {
                return;
            }

            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var redirectUrl = urlHelper.Action(checkoutPage, routeValues: new {action = nameof(CheckoutController.PlaceOrderDirect)});
            var routeValues = new
            {
                PaymentProvider = "DirectPay",
                PaymentMethod = "DirectPay",
                RedirectUrl = redirectUrl
            };

            var cashOnDeliveryExternalPayment = new ExternalPaymentMethod
            {
                Name = "Cash on delivery",
                RedirectUri = new Uri(urlHelper.Action("ChangePaymentMethod", "KlarnaPayment", routeValues, Uri.UriSchemeHttps)),
                Fee = 0
            };

            klarnaCheckoutOrder.ExternalPaymentMethods = new List<ExternalPaymentMethod>
            {
                cashOnDeliveryExternalPayment
            };
        }
    }
}