﻿using System.Web.Mvc;
using Litium.Accelerator.Builders.Menu;
using Litium.Web.Runtime;

namespace Litium.Accelerator.Mvc.Controllers
{
    /// <summary>
    /// Controller base class that helps out to set correct layout for rendering.
    /// </summary>
    /// <seealso cref="System.Web.Mvc.Controller" />
    public abstract class ControllerBase : Controller
    {
        [FromService]
        public MenuViewModelBuilder MenuViewModelBuilder { get; set; }

        protected override ViewResult View(string viewName, string masterName, object model)
        {
            if (string.IsNullOrEmpty(masterName))
            {
                var menuModel = MenuViewModelBuilder.Build();
                masterName = menuModel.ShowLeftColumn
                    ? "~/Views/Shared/_LayoutWithLeftColumn.cshtml"
                    : "~/Views/Shared/_Layout.cshtml";
            }
            return base.View(viewName, masterName, model);
        }
    }
}