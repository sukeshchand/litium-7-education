﻿using Litium.Accelerator.ViewModels;
using Litium.Web.Models.Products;
using System.Web.Mvc;
using Litium.Accelerator.Builders.Product;

namespace Litium.Accelerator.Mvc.Controllers.Category
{
    public class CategoryController : ControllerBase
    {
        private readonly CategoryPageViewModelBuilder _categoryPageViewModelBuilder;
        private readonly ChildCategoryNavigationBuilder _childCategoryNavigationBuilder;

        public CategoryController(CategoryPageViewModelBuilder categoryPageViewModelBuilder, ChildCategoryNavigationBuilder childCategoryNavigationBuilder)
        {
            _childCategoryNavigationBuilder = childCategoryNavigationBuilder;
            _categoryPageViewModelBuilder = categoryPageViewModelBuilder;
        }

        public ActionResult Index(CategoryModel currentCategoryModel)
        {
            var model = _categoryPageViewModelBuilder.Build(currentCategoryModel.SystemId);

            return View(model);
        }

        [ChildActionOnly]
        public ActionResult ChildCategoryNavigation(CategoryModel currentCategory)
        {
            var model = _childCategoryNavigationBuilder.Build(currentCategory);
            if (model != null)
            {
                return PartialView(model);
            }

            return null;
        }
    }
}