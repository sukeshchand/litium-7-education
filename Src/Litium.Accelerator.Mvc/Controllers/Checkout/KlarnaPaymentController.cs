﻿using Klarna.Rest.Models;
using Litium.Accelerator.Mvc.Attributes;
using Litium.Accelerator.Payments.Widgets;
using Litium.Accelerator.Services;
using Litium.Accelerator.Services.Payments;
using Litium.Accelerator.ViewModels.Checkout;
using Litium.AddOns.Klarna.Abstractions;
using Litium.AddOns.Klarna.ExtensionMethods;
using Litium.Foundation.Modules.ECommerce.ShoppingCarts;
using System;
using System.Linq;
using System.Web.Mvc;
using KlarnaV3 = Litium.AddOns.Klarna;

namespace Litium.Accelerator.Mvc.Controllers.Checkout
{
    public class KlarnaPaymentController : Controller
    {
        private readonly IKlarnaPaymentConfig _paymentWidgetConfig;
        private readonly KlarnaPaymentService _klarnaPaymentService;
        private readonly CheckoutService _checkoutService;
        private readonly CartAccessor _cartAccessor;

        public KlarnaPaymentController(
            IKlarnaPaymentConfig paymentWidgetConfig,
            CartAccessor cartAccessor,
            KlarnaPaymentService klarnaPaymentService,
            CheckoutService checkoutService)
        {
            _paymentWidgetConfig = paymentWidgetConfig;
            _klarnaPaymentService = klarnaPaymentService;
            _checkoutService = checkoutService;
            _cartAccessor = cartAccessor;
        }

        [HttpGet]
        public ActionResult Confirmation(string accountId, string transactionNumber)
        {
            this.Log().Debug("Confirmation started accountId {accountId} transactionNumber {transactionNumber}.", accountId, transactionNumber);
            try
            {
                var klarnaCheckout = _klarnaPaymentService.CreateKlarnaCheckoutApi(accountId);
                var checkoutOrder = klarnaCheckout.FetchKcoOrder(transactionNumber.Split('/').Last());
                if (checkoutOrder != null)
                {
                    _klarnaPaymentService.PlaceOrder(klarnaCheckout, checkoutOrder, true);
                    this.Log().Debug("Confirmation completed.");
                }
            }
            catch (Exception ex)
            {
                var logger = this.WebLog();
                logger.Append("account id", accountId);
                logger.Append("transaction number", transactionNumber);
                logger.Error(ex);
            }

            var redirectUrl = _checkoutService.GetOrderConfirmationPageUrl(_cartAccessor.Cart.OrderCarrier);
            return Redirect(redirectUrl);
        }

        [HttpPost]
        public ActionResult Validate(string accountId, string transactionNumber)
        {
            var klarnaCheckout = _klarnaPaymentService.CreateKlarnaCheckoutApi(accountId);
            klarnaCheckout.ValidateKcoOrder(order => _paymentWidgetConfig.ValidateCheckoutOrder(order), Request, Response);
            return Content("OK");
        }

        [HttpPost]
        public ActionResult AddressUpdate(string accountId, string transactionNumber, [FromJsonBody] CheckoutOrderData checkoutOrderData)
        {
            this.Log().Debug("Address update started accountId {accountId} transactionNumber {transactionNumber}.", accountId, transactionNumber);
            _paymentWidgetConfig.AddressUpdate(checkoutOrderData);

            this.Log().Debug("Completed.");
            //if the correct Json result is not written to Klarna, the checkout snippet will show and error!.
            return new JsonResult { Data = checkoutOrderData };
        }

        [HttpGet]
        public ActionResult ChangePaymentMethod(string paymentProvider, string paymentMethod, string redirectUrl)
        {
            var cart = _cartAccessor.Cart;
            var paymentInfoCarrier = cart.OrderCarrier.PaymentInfo.FirstOrDefault(x => x.PaymentProvider == KlarnaV3.KlarnaProvider.ProviderName);
            if (!string.IsNullOrEmpty(paymentInfoCarrier?.PaymentMethod))
            {
                var paymentAccountId = _klarnaPaymentService.GetPaymentAccountId(paymentInfoCarrier.PaymentMethod);
                var klarnaCheckout = _klarnaPaymentService.CreateKlarnaCheckoutApi(paymentAccountId);
                var checkoutOrder = klarnaCheckout.FetchKcoOrder(cart.OrderCarrier);
                if (checkoutOrder?.OrderCarrier != null)
                {
                    paymentInfoCarrier = checkoutOrder.OrderCarrier.PaymentInfo.FirstOrDefault(x => x.PaymentProvider == KlarnaV3.KlarnaProvider.ProviderName);
                    cart.OrderCarrier = checkoutOrder.OrderCarrier;
                }
            }

            if (paymentInfoCarrier != null)
            {
                paymentInfoCarrier.PaymentProvider = paymentProvider;
                paymentInfoCarrier.PaymentMethod = paymentMethod;
            }
            return Redirect(redirectUrl);
        }

        [HttpPost]
        public ActionResult KlarnaOnChange(KlarnaOnChangeViewModel args)
        {
            var paymentInfoCarrier = _cartAccessor.Cart.OrderCarrier.PaymentInfo.First(x => x.PaymentProvider == KlarnaV3.KlarnaProvider.ProviderName);
            if (!string.IsNullOrEmpty(paymentInfoCarrier?.PaymentMethod))
            {
                var paymentAccountId = _klarnaPaymentService.GetPaymentAccountId(paymentInfoCarrier.PaymentMethod);
                var klarnaCheckout = _klarnaPaymentService.CreateKlarnaCheckoutApi(paymentAccountId);
                _klarnaPaymentService.SetAddress(args, paymentInfoCarrier.BillingAddress);
                klarnaCheckout.CustomerDetailsChanged(paymentInfoCarrier.GetKlarnaOrderId(), new CustomerDetails
                {
                    Country = args.Country,
                    Email = args.Email,
                    FirstName = args.GivenName,
                    LastName = args.FamilyName,
                    Zip = args.PostalCode
                });
            }

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult PushNotification(string accountId, string transactionNumber)
        {
            this.Log().Debug("Push notification started accountId {accountId} transactionNumber {transactionNumber}.", accountId, transactionNumber);
            var klarnaCheckout = _klarnaPaymentService.CreateKlarnaCheckoutApi(accountId);
            var checkoutOrder = klarnaCheckout.FetchKcoOrder(transactionNumber);

            //to place the order, we need both the order carrier and the klarna checkout order.
            if (checkoutOrder?.OrderCarrier != null)
            {
                _klarnaPaymentService.PlaceOrder(klarnaCheckout, checkoutOrder, false);
                this.Log().Debug("Push notification completed.");
                return Content("OK");
            }
            else
            {
                this.Log().Debug("No order was found for push notification.");
                return Content("Error");
            }
        }
    }
}
