﻿using Litium.Accelerator.Builders.Checkout;
using Litium.Accelerator.Constants;
using Litium.Accelerator.Mvc.Attributes;
using Litium.Accelerator.Mvc.ModelStates;
using Litium.Accelerator.Routing;
using Litium.Accelerator.Services;
using Litium.Accelerator.ViewModels.Checkout;
using Litium.FieldFramework.FieldTypes;
using Litium.Foundation.Modules.ECommerce.Plugins.Utilities;
using Litium.Studio.Extenssions;
using Litium.Web;
using Litium.Websites;
using System;
using System.Web.Http;

namespace Litium.Accelerator.Mvc.Controllers.Api
{
    [RoutePrefix("api/checkout")]
    public class CheckoutController : ApiControllerBase
    {
        private readonly CheckoutService _checkoutService;
        private readonly PaymentMethodViewModelBuilder _paymentMethodViewModelBuilder;
        private readonly RequestModelAccessor _requestModelAccessor;
        private readonly PageService _pageService;
        private readonly UrlService _urlService;

        public CheckoutController(
            CheckoutService checkoutService,
            PaymentMethodViewModelBuilder paymentMethodViewModelBuilder,
            UrlService urlService,
            PageService pageService,
            RequestModelAccessor requestModelAccessor)
        {
            _checkoutService = checkoutService;
            _paymentMethodViewModelBuilder = paymentMethodViewModelBuilder;
            _requestModelAccessor = requestModelAccessor;
            _urlService = urlService;
            _pageService = pageService;
        }

        [HttpPost]
        [ApiValidateAntiForgeryToken]
        public IHttpActionResult PlaceOrder(CheckoutViewModel model)
        {
            if (!_checkoutService.Validate(new ApiModelState(ModelState), model))
            {
                return BadRequest(ModelState);
            }

            if (!_checkoutService.ValidateOrder(out string message))
            {
                ModelState.AddModelError("general", message);
                return BadRequest(ModelState);
            }

            try
            {
                var checkoutPage = _requestModelAccessor.RequestModel.WebsiteModel.Fields.GetValue<PointerPageItem>(AcceleratorWebsiteFieldNameConstants.CheckouPage);
                var checkoutPageUrl = GetAbsolutePageUrl(checkoutPage);
                var responseUrl = new QueryString()
                        .Add(CheckoutConstants.QueryStringStep, CheckoutConstants.Response)
                        .ReplaceQueryString($"{checkoutPageUrl}.HandleResponse");
                var cancelUrl = new QueryString()
                        .Add(CheckoutConstants.QueryStringStep, CheckoutConstants.Cancel)
                        .ReplaceQueryString($"{checkoutPageUrl}.HandleCancelResponse");
                var executePaymentResult = _checkoutService.PlaceOrder(model, responseUrl, cancelUrl, out string redirectUrl);
                if (executePaymentResult == null)
                {
                    return null;
                }
                if (!string.IsNullOrEmpty(redirectUrl))
                {
                    model.RedirectUrl = redirectUrl;
                    return Ok(model);
                }

                redirectUrl = _checkoutService.HandlePaymentResult(executePaymentResult.Success, executePaymentResult.ErrorMessage);
                model.RedirectUrl = redirectUrl;
                return Ok(model);
            }
            catch (Exception ex)
            {
                this.Log().Error("Error when placing an order", ex);
                ModelState.AddModelError("general", "checkout.generalerror".AsWebSiteString());
                return BadRequest(ModelState);
            }
        }

        [HttpPut]
        [ApiValidateAntiForgeryToken]
        [Route("setPaymentProvider")]
        public IHttpActionResult SetPaymentProvider(CheckoutViewModel model)
        {
            try
            {
                _checkoutService.ChangePaymentMethod(model.SelectedPaymentMethod);
                model.PaymentWidget = _paymentMethodViewModelBuilder.BuildWidget(model.SelectedPaymentMethod);
                return Ok(model);
            }
            catch (Exception ex)
            {
                this.Log().Error("Error when changing payment provider", ex);
                ModelState.AddModelError("general", "checkout.setpaymenterror".AsWebSiteString());
                return BadRequest(ModelState);
            }
        }

        [HttpPut]
        [ApiValidateAntiForgeryToken]
        [Route("setDeliveryProvider")]
        public IHttpActionResult SetDeliveryProvider(CheckoutViewModel model)
        {
            try
            {
                _checkoutService.ChangeDeliveryMethod(model.SelectedDeliveryMethod.Value);
                return Ok(model);
            }
            catch (Exception ex)
            {
                this.Log().Error("Error when changing delivery provider", ex);
                ModelState.AddModelError("general", "checkout.setdeliveryerror".AsWebSiteString());
                return BadRequest(ModelState);
            }
        }

        [HttpPut]
        [ApiValidateAntiForgeryToken]
        [Route("setCampaignCode")]
        public IHttpActionResult SetCampaignCode(CheckoutViewModel model)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(model.CampaignCode) || _checkoutService.SetCampaignCode(model.CampaignCode))
                {
                    return Ok(model);
                }

                ModelState.AddModelError(nameof(CheckoutViewModel.CampaignCode), "checkout.campaigncodeinvalid".AsWebSiteString());
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                this.Log().Error("Error when setting campaign code", ex);
                ModelState.AddModelError("general", "checkout.setcampaigncodeerror".AsWebSiteString());
                return BadRequest(ModelState);
            }
        }

        private string GetAbsolutePageUrl(PointerPageItem pointer)
        {
            if (pointer == null)
            {
                return null;
            }
            var page = _pageService.Get(pointer.EntitySystemId);
            if (page == null)
            {
                return null;
            }

            var channelSystemId = pointer.ChannelSystemId != Guid.Empty ? pointer.ChannelSystemId : _requestModelAccessor.RequestModel.ChannelModel.SystemId;
            return _urlService.GetUrl(page, new PageUrlArgs(channelSystemId) { AbsoluteUrl = true });
        }
    }
}