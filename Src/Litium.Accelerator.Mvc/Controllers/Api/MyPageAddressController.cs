﻿using Litium.Accelerator.Mvc.Attributes;
using Litium.Accelerator.Mvc.ModelStates;
using Litium.Accelerator.Services;
using Litium.Accelerator.ViewModels.Persons;
using System;
using System.Web.Http;
using Litium.Accelerator.Builders.Person;

namespace Litium.Accelerator.Mvc.Controllers.Api
{
    [OrganizationRole(true, false)]
    public class MyPageAddressController : ApiControllerBase
    {
        private readonly BusinessAddressViewModelBuilder _businessAddressViewModelBuilder;
        private readonly AddressViewModelService _addressViewModelService;

        private readonly ModelState _modelState;

        public MyPageAddressController(BusinessAddressViewModelBuilder businessAddressViewModelBuilder, AddressViewModelService addressViewModelService)
        {
            _businessAddressViewModelBuilder = businessAddressViewModelBuilder;
            _addressViewModelService = addressViewModelService;

            _modelState = new ApiModelState(ModelState);
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            var model = _businessAddressViewModelBuilder.Build();
            return Ok(model);
        }

        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            var model = _businessAddressViewModelBuilder.Build(id);
            return Ok(model);
        }

        [HttpPost]
        [ApiValidateAntiForgeryToken]
        public IHttpActionResult Add(AddressViewModel viewModel)
        {
            if (_addressViewModelService.Validate(viewModel, _modelState) && 
                _addressViewModelService.Create(viewModel, _modelState))
            {
                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpPut]
        [ApiValidateAntiForgeryToken]
        public IHttpActionResult Update(AddressViewModel viewModel)
        {
            if (_addressViewModelService.Validate(viewModel, _modelState) && _addressViewModelService.Update(viewModel, _modelState))
            {
                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpDelete]
        [ApiValidateAntiForgeryToken]
        public IHttpActionResult Delete([FromBody]Guid personId)
        {
            _addressViewModelService.Delete(personId);
            return Ok();
        }
    }
}