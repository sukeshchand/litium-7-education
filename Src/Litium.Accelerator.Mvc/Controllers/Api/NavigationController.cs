﻿using System.Web.Http;
using Litium.Accelerator.Builders.Framework;

namespace Litium.Accelerator.Mvc.Controllers.Api
{
    public class NavigationController : ApiControllerBase
    {
        private readonly NavigationViewModelBuilder _navigationViewModelBuilder;

        public NavigationController(NavigationViewModelBuilder navigationViewModelBuilder)
        {
            _navigationViewModelBuilder = navigationViewModelBuilder;
        }

        /// <summary>
        /// Get navigation menu
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(_navigationViewModelBuilder.Build());
        }
    }
}