﻿using System.Web.Http;

namespace Litium.Accelerator.Mvc.Controllers.Api
{
    /// <summary>
    /// Api controller base class.
    /// </summary>
    public abstract class ApiControllerBase : ApiController
    {
    }
}