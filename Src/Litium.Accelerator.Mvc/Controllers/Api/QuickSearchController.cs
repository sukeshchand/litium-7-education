﻿using Litium.Accelerator.Mvc.Attributes;
using System.Web.Http;
using Litium.Accelerator.Builders.Search;

namespace Litium.Accelerator.Mvc.Controllers.Api
{
    public class QuickSearchController : ApiControllerBase
    {
        private readonly QuickSearchResultViewModelBuilder _quickSearchResultViewModelBuilder;

        public QuickSearchController(QuickSearchResultViewModelBuilder quickSearchResultViewModelBuilder)
        {
            _quickSearchResultViewModelBuilder = quickSearchResultViewModelBuilder;
        }

        [HttpPost]
        [ApiValidateAntiForgeryToken]
        public IHttpActionResult Post([FromBody]string query)
        {
            var result = _quickSearchResultViewModelBuilder.Build(query);
            return Ok(result.Results);
        }
    }
}