﻿using System;
using System.Web.Http;
using Litium.Accelerator.Builders.Person;
using Litium.Accelerator.Mvc.Attributes;
using Litium.Accelerator.Mvc.ModelStates;
using Litium.Accelerator.Services;
using Litium.Accelerator.ViewModels.Persons;

namespace Litium.Accelerator.Mvc.Controllers.Api
{
    [OrganizationRole(true, false)]
    public class MyPagePersonController : ApiControllerBase
    {
        private readonly BusinessPersonViewModelBuilder _b2BPersonViewModelBuilder;
        private readonly PersonViewModelService _personViewModelService;

        private readonly ModelState _modelState;

        public MyPagePersonController(BusinessPersonViewModelBuilder b2BPersonViewModelBuilder, 
            PersonViewModelService personViewModelService)
        {
            _b2BPersonViewModelBuilder = b2BPersonViewModelBuilder;
            _personViewModelService = personViewModelService;

            _modelState = new ApiModelState(ModelState);
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            var model = _b2BPersonViewModelBuilder.Build();
            return Ok(model);
        }

        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            var model = _b2BPersonViewModelBuilder.Build(id);
            return Ok(model);
        }

        [HttpPut]
        [ApiValidateAntiForgeryToken]
        public IHttpActionResult Update(BusinessPersonViewModel viewModel)
        {
            if (_personViewModelService.Validate(viewModel, _modelState) 
                && _personViewModelService.Update(viewModel, _modelState))
            {
                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpDelete]  
        [ApiValidateAntiForgeryToken]
        public IHttpActionResult Delete([FromBody]Guid personId)
        {
            _personViewModelService.Delete(personId);
            return Ok();
        }

        [HttpPost]
        [ApiValidateAntiForgeryToken]
        public IHttpActionResult Add(BusinessPersonViewModel viewModel)
        {
            if (_personViewModelService.Validate(viewModel, _modelState) 
                && _personViewModelService.Create(viewModel, _modelState))
            {
                return Ok();
            }

            return BadRequest(ModelState);
        }
    }
}