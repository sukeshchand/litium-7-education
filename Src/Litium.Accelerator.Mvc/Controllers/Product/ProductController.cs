﻿using System.Linq;
using System.Web.Mvc;
using Litium.Accelerator.Builders.Product;
using Litium.Foundation.Modules.CMS.WebSites;
using Litium.Products;
using Litium.Web.Models.Products;

namespace Litium.Accelerator.Mvc.Controllers.Product
{
    public class ProductController : ControllerBase
    {
        private readonly ProductPageViewModelBuilder _productPageViewModelBuilder;

        public ProductController(ProductPageViewModelBuilder productPageViewModelBuilder)
        {
            _productPageViewModelBuilder = productPageViewModelBuilder;
        }

        [HttpGet]
        public ActionResult ProductWithVariants(CategoryModel categoryModel, Variant variant)
        {
            var productPageModel = _productPageViewModelBuilder.Build(categoryModel, variant);
            return View(productPageModel);
        }

        [HttpGet]
        public ActionResult ProductWithVariantListing(CategoryModel categoryModel, BaseProduct baseProduct)
        {
            var productPageModel = _productPageViewModelBuilder.Build(categoryModel, baseProduct);
            return View(productPageModel);
        }
    }
}