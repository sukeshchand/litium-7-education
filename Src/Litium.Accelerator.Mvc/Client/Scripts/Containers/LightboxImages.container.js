import React, { Component } from 'react';
import { connect } from 'react-redux';
import Lightbox from 'react-images';
import { setImages, setCurrentIndex, show, previous, next } from '../Actions/LightboxImages.action';

class LightboxImagesContainer extends Component {
    render() {
        return <Lightbox 
            showThumbnails={true}
            showImageCount={false}
            {...this.props}
        />
    }
}
const mapStateToProps = (state) => {
    return {
        images: state.lightboxImages.images,
        isOpen: state.lightboxImages.visible,
        currentImage: state.lightboxImages.index,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onClose: () => dispatch(show(false)),
        onClickNext: () => dispatch(next()),
        onClickPrev: () => dispatch(previous()),
        onClickThumbnail: (index) => dispatch(setCurrentIndex(index)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LightboxImagesContainer);