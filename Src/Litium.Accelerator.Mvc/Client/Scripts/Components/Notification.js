import React from 'react';

const Notification = ({ text, top, left }) => (
    <span className='buy-button__tooltip' style={{ top: `${top}px`, left: `${left}px` }}>{text}</span>
);

export default Notification;