import React, { Fragment } from 'react';

const Navigation = ({links=[], contentLink=null }) => {
    let menuRef = React.createRef();
    function toggleMenu() {
        menuRef.current.classList.toggle('navbar__menu--show');
    }
    const additionClass = contentLink && contentLink.attributes ? contentLink.attributes.cssValue : null;
    const hasChildrenClass = links.length > 0 ? 'has-children' : null;

    return (
    <Fragment>
        {!contentLink ? (
            <a className="navbar__link--block navbar__icon--menu navbar__icon" onClick={toggleMenu}></a>
        ) : (
            <Fragment>
                <a className={`navbar__link ${hasChildrenClass || '' } ${additionClass || ''}`} 
                    href={contentLink.url||'#'} dangerouslySetInnerHTML={{__html: contentLink.name}} >
                </a>
                {links.length > 0 &&
                    <i className="navbar__icon--caret-right navbar__icon navbar__icon--open" onClick={toggleMenu}></i>
                }
            </Fragment>
        )}

        { links.length > 0 &&
            <ul className="navbar__menu" ref={menuRef} test={links.length}>
                <div className="navbar__menu-header">
                    {!contentLink ? (
                        <span className="navbar__icon navbar__icon--close" onClick={toggleMenu}></span>
                    ) : (
                        <Fragment>
                            <i className="navbar__icon--caret-left navbar__icon" onClick={toggleMenu}></i>
                            <span className="navbar__title" onClick={toggleMenu} dangerouslySetInnerHTML={{__html: contentLink.name}}></span>
                        </Fragment>
                    )}
                </div>
                {links.length > 0 && links.map((link, i) => !link.name ? null :
                    <li className="navbar__item" key={`${link.name}-${i}`}>
                        <Navigation links={link.links} contentLink={link}/>
                    </li>
                )}
            </ul>
        }
        
    </Fragment>
)}

export default Navigation;