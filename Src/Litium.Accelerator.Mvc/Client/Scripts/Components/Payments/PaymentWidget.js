import React, { Component, Fragment } from 'react';
import DynamicComponent from '../DynamicComponent';
const scriptPattern = /<script\b[^>]*>([\s\S]*?)<\/script>/gi;

class PaymentWidget extends Component {
    render() {
        const { id, responseString } = this.props;
        if (id === 'KlarnaCheckout') {
            return this.renderKlarna(responseString);
        }
        return <span />;
    }

    renderKlarna(paymentSession) {
        const KlarnaCheckout = DynamicComponent({
            loader: () => import('./KlarnaCheckout')
        });
        const args = { paymentSession, extractScripts: this.extractScripts, executeScript: this.executeScript };
        return (
            <KlarnaCheckout { ...args } />
        );
    }

    extractScripts(domString) {
        var matches, html = domString;
        const scripts = [];
        while ((matches = scriptPattern.exec(domString)) !== null) {
            html = html.replace(matches[0], '');
            scripts.push(matches[1]);
        }
        return {
            html,
            scripts,
        };
    }

    executeScript(domId, scriptContent) {
        const script = document.createElement("script");
        script.type = "text/javascript";
        try {
            script.appendChild(document.createTextNode(scriptContent));      
        } catch(e) {
            // to support IE
            script.text = scriptContent;
        }
        document.getElementById(domId).appendChild(script);
    }
}

export default PaymentWidget;