import React, { Component } from 'react';

class KlarnaCheckout extends Component {
    constructor(props) {
        super(props);
        this.state = props.extractScripts(props.paymentSession);
    }

    componentDidMount() {
        this.state.scripts && this.state.scripts.forEach(script => this.props.executeScript('klarna-checkout', script));
    }

    render() {
        return <div id="klarna-checkout" dangerouslySetInnerHTML={{ __html: this.state.html }} />;
    }
}

export default KlarnaCheckout;