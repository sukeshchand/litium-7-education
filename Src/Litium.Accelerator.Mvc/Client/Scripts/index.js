import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, bindActionCreators, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';
import app from './reducers';
import { historyMiddleware } from './Middlewares/History.middleware';
import { add as addToCart, reorder } from './Actions/Cart.action';
import MiniCartContainer from './Containers/MiniCart.container';
import QuickSearchContainer from './Containers/QuickSearch.container';
import NavigationContainer from './Containers/Navigation.container';
import FacetedSearchContainer from './Containers/FacetedSearch.container';
import FacetedSearchCompactContainer from './Containers/FacetedSearchCompact.container';
import NotificationContainer from './Containers/Notification.container';
import DynamicComponent from './Components/DynamicComponent';
import {
    setImages as setProductImageSlideShows, 
    show as showProductImageSlideShow, 
    setCurrentIndex as setProductImageSlideShowIndex 
} from './Actions/LightboxImages.action';
import { translate } from './Services/translation';

window.__litium = window.__litium || {};
const preloadState = window.__litium.preloadState || {};
const store = createStore(app, preloadState, composeWithDevTools(applyMiddleware(thunk, historyMiddleware)));
// register redux's actions so we can invoke them from vanilla JS
// usage: window.__litium.reduxActions.addToCart(...);
window.__litium = {
    ...window.__litium,
    reduxActions: bindActionCreators({ addToCart, reorder, setProductImageSlideShows, showProductImageSlideShow, setProductImageSlideShowIndex} , store.dispatch),
    events: {
        onAddToCartButtonClick: (sourceDomNode, articleNumber, quantityFieldId = null) => {
            const nodeIdToShowNotification = sourceDomNode.id = Date.now();
            window.__litium.reduxActions.addToCart({ 
                articleNumber, 
                quantity: quantityFieldId ? document.getElementById(quantityFieldId).value : 1,
                nodeIdToShowNotification,
                notificationMessage: translate('tooltip.addedtocart'),
                hash: Date.now(),
            });
        },
        onReorderClick: (orderid) => {
            if (orderid) {
                window.__litium.reduxActions.reorder({orderid});
            }
        },
        onProductImageClick: ele => {
            const productImagesSelector = 'product-images';
            const productImageSelector = 'product-image';
            const closest = (el, className) => el ? (el.classList.contains(className) ? el : closest(el.parentNode, className)) : null;
            const parentNodes = ele.closest ? ele.closest(`.${productImagesSelector}`) : closest(ele, productImagesSelector);
            const images = Array.from(parentNodes.querySelectorAll(`.${productImageSelector}`)).map(img => ({src : img.dataset.src}));
            const index = images.findIndex(_ele => ele.dataset.src === _ele.src);

            window.__litium.reduxActions.setProductImageSlideShows(images);
            window.__litium.reduxActions.showProductImageSlideShow(true);
            window.__litium.reduxActions.setProductImageSlideShowIndex(index);
        }
    },
    bootstrapComponents: () => {
        // bootstrap React components, in case the HTML response we receive from the server
        // has React components. ReactDOM.render performs only an update on previous rendered
        // components and only mutate the DOM as necessary to reflect latest React element.
        bootstrapComponents();
    },
    cache: {}, // for storing cache data for current request
};

const bootstrapComponents = () => {
    if (document.getElementById('miniCart')) {
        ReactDOM.render(
            <Provider store={store}>
                <MiniCartContainer/>
            </Provider>,
    document.getElementById('miniCart')
        );
    }
    if (document.getElementById('quickSearch')) {
        ReactDOM.render(
            <Provider store={store}>
                <QuickSearchContainer/>
            </Provider>,
    document.getElementById('quickSearch')
            );
    }
    if (document.getElementById('navbar')) {
        ReactDOM.render(
            <Provider store={store}>
                <NavigationContainer/>
            </Provider>,
    document.getElementById('navbar')
            );
    }
    if (document.getElementById('facetedSearch')) {
        ReactDOM.render(
            <Provider store={store}>
                <FacetedSearchContainer/>
            </Provider>,
    document.getElementById('facetedSearch')
            );
    }
    if (document.getElementById('facetedSearchCompact')) {
        ReactDOM.render(
            <Provider store={store}>
                <FacetedSearchCompactContainer/>
            </Provider>,
    document.getElementById('facetedSearchCompact')
            );
    }
    if (document.getElementById('globalNotification')) {
        ReactDOM.render(
            <Provider store={store}>
                <NotificationContainer/>
            </Provider>,
    document.getElementById('globalNotification')
            );
    }

    if (document.getElementById('myPagePersons')) {
        const PersonList =  DynamicComponent({
            loader: () => import('./Containers/PersonList.container')
        });
        ReactDOM.render(
            <Provider store={store}>
                <PersonList />
            </Provider>,
    document.getElementById('myPagePersons')
        );
    }
    if (document.getElementById('myPageAddresses')) {
        const AddressList = DynamicComponent({
            loader: () => import('./Containers/AddressList.container')
        });
        ReactDOM.render(
            <Provider store={store}>
                <AddressList />
            </Provider>,
    document.getElementById('myPageAddresses')
        );
    }
    if (document.getElementById('checkout')) {
        const Checkout = DynamicComponent({
            loader: () => import('./Containers/Checkout.container')
        });
        ReactDOM.render(
            <Provider store={store}>
                <Checkout />
            </Provider>,
    document.getElementById('checkout')
        );
    }
    if (document.getElementById('lightBoxImages')) {
        const LightboxImages = DynamicComponent({
            loader: () => import('./Containers/LightboxImages.container')
        });
        ReactDOM.render(
            <Provider store={store}>
                <LightboxImages/>
            </Provider>,
    document.getElementById('lightBoxImages')
        );
    }

    if (document.querySelectorAll('.slider').length>0) {
        const Slider = DynamicComponent({
            loader: () => import('./Components/Slider')
        });
        Array.from(document.querySelectorAll('.slider')).forEach((slider, index) => {
            const values = [...slider.querySelectorAll('.slider__block')].map(block=>{
                return ({
                    image : block.dataset.image,
                    url : block.dataset.url,
                    text : block.dataset.text,
                })
            });
            if (values.length > 0) {
                ReactDOM.render(
                    <Slider values={values}/>,
            slider
                );
        }
        });
    }
}

bootstrapComponents();