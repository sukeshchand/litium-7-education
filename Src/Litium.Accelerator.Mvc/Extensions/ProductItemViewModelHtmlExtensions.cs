﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using Litium.Runtime;
using Litium.Accelerator.ViewModels.Product;
using Litium.Studio.Extenssions;

namespace Litium.Accelerator.Mvc.Extensions
{
    public static class ProductItemViewModelHtmlExtensions
    {
        public static MvcHtmlString BuyButton<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, ProductItemViewModel>> expression, string cssClass = null, bool? isBuyButton = null)
        {
            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            return htmlHelper.BuyButton((ProductItemViewModel)modelMetadata.Model, cssClass, isBuyButton);
        }

        public static MvcHtmlString BuyButton(this HtmlHelper<ProductItemViewModel> htmlHelper, string cssClass = null, bool? isBuyButton = null)
        {
            return htmlHelper.BuyButton(htmlHelper.ViewData.Model, cssClass, isBuyButton);
        }

        private static MvcHtmlString BuyButton(this HtmlHelper htmlHelper, ProductItemViewModel model, string cssClass, bool? isBuyButton)
        {
            var buy = model.ShowBuyButton && (isBuyButton.HasValue && isBuyButton.Value || model.UseVariantUrl);
            var tag = new TagBuilder("a")
            {
                InnerHtml = buy ? "product.buy".AsWebSiteString() : "product.show".AsWebSiteString()
            };

            cssClass = $"button {(buy ? "buy-button" : "show-button")} {cssClass ?? string.Empty}".Trim();
            if (model.ShowBuyButton)
            {
                if (buy)
                {
                    var enabled = !string.IsNullOrEmpty(model.Url) && model.Price.HasPrice && model.IsInStock;
                    if (enabled)
                    {
                        var quantityFieldId = model.ShowQuantityField ? $"'{model.QuantityFieldId}'" : "null";
                        tag.Attributes.Add("onclick", $"window.__litium.events.onAddToCartButtonClick(this,'{model.Id}', {quantityFieldId} )");
                    }
                    else
                    {
                        cssClass += " disabled";
                    }
                }
                else
                {
                    tag.Attributes.Add("href", model.Url);
                }
            }
            else if (string.IsNullOrEmpty(model.Url))
            {
                cssClass += " disabled";
            }

            if (!string.IsNullOrWhiteSpace(cssClass))
            {
                tag.Attributes.Add("class", cssClass);
            }

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }
    }
}