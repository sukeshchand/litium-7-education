﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Hosting;
using Litium.Foundation.Configuration;
using Litium.Owin.Lifecycle;
using Litium.Web.Administration;

namespace Litium.Accelerator.WebForm.Site.Administration.Settings
{
    internal class SettingsPageSetup : IStartupTask, IAppExtension
    {
        void IStartupTask.Start()
        {
            List<ControlPanelPage> pages;
            if (!ControlPanelPagesConfig.Instance.Groups.TryGetValue("Accelerator", out pages))
            {
                ControlPanelPagesConfig.Instance.Groups.Add("Accelerator", pages = new List<ControlPanelPage>());
            }
            pages.Add(new ControlPanelPage("~/Litium/Framework/Frame.aspx/Litium/appSettings/accelerator/indexing", "AcceleratorIndexingSetting", ControlPanelPagePermission.All)
            {
                //AuthorizeFunc = () => FoundationContext.Current.User.HasModulePermission(ModuleCMS.Instance.ID, BuiltInModulePermissionTypes.PERMISSION_ID_ALL, true, true)
            });
            pages.Add(new ControlPanelPage("~/Litium/Framework/Frame.aspx/Litium/appSettings/accelerator/filter", "AcceleratorFilterSetting", ControlPanelPagePermission.All)
            {
                //AuthorizeFunc = () => FoundationContext.Current.User.HasModulePermission(ModuleCMS.Instance.ID, BuiltInModulePermissionTypes.PERMISSION_ID_ALL, true, true)
            });
        }

        IEnumerable<string> IAppExtension.ScriptFiles => HostingEnvironment.VirtualPathProvider.GetDirectory("/Site/Administration/Settings")
            .Files.OfType<VirtualFile>()
            .Where(x => x.VirtualPath.EndsWith(".js"))
            .Select(x => x.VirtualPath);

        IEnumerable<string> IAppExtension.AngularModules { get; }
        IEnumerable<string> IAppExtension.StylesheetFiles { get; }
    }
}